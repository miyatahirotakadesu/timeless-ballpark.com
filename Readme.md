# 実在プロ野球選手育成シミュレーションゲーム  
  
### 開発環境 Laravel5 + Vue.js  
#### URL:https://wikinpb.com/    
#### 開発工数：約１６０時間  
#### 開発時期: 2017年9月〜10月  

wikipediaに載っているプロ野球選手のデータを使って野球チームを作るゲーム。実在選手と同じように選手は歳をとって選手成績が変化していく。   
SPAアプリケーションであり、UIはスマホソシャゲを意識（真似）して作成した。
試合実行部分だけ、c#で作られた【野球自動試合実行シミュレーター】を使っている。  

Vue.jsのコードは
https://bitbucket.org/miyatahirotakadesu/timeless-ballpark.com/src/3f02e1165bafeb996737b06626792860c65a9421/resources/assets/?at=master
にあります。