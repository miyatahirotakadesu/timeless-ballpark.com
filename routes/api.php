<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api']], function () {
    Route::post('/user/signup', 'UserController@signup');
    Route::post('/user/login', 'UserController@login');
    Route::get('/auth/user', 'UserController@auth');
    Route::get('/auth/team', 'TeamController@auth');
    Route::get('/team/{id}', 'TeamController@show');
    Route::post('/team', 'TeamController@store');
    Route::get('/mail', 'MailController@index');
    Route::post('/mail/read', 'MailController@read');
    Route::post('/mail/gift/receive', 'MailController@receive_gift');
    Route::post('/mail/bulk', 'MailController@bulk');
    Route::get('/game', 'GameController@index')->middleware('gametime');
    Route::get('/game/{id}', 'GameController@show');
    Route::post('/scout', 'PlayerController@scout');
    Route::get('/active_player', 'PlayerController@active_player');
    Route::get('/possessed_player', 'PlayerController@possessed_player');
    Route::get('/possessed_player/{id}', 'PlayerController@show_possessed_player');
    Route::get('/team_order', 'TeamController@show_order');
    Route::post('/team_order', 'TeamController@update_order');
    Route::get('/directory', 'PlayerController@directory');
    Route::post('/possessed_player/skip', 'PlayerController@skip');
    Route::get('/ranking', 'RankingController@index')->middleware('gametime');
    Route::post('/team/name', 'TeamController@update_name');
    Route::get('/auth_type', 'UserController@auth_type');
    Route::post('/auth/logout', 'UserController@logout');
    Route::get('/system_param', 'SystemParamController@index');
});
