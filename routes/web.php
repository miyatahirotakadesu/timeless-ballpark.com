<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['check_introducing_code']], function () {
    Route::get('/', function () { return view('app'); });
    Route::get('/game/{id}', function () { return view('app'); });
});

Route::get('/login', function () { return view('app'); });
Route::get('/menu', function () { return view('app'); });
Route::get('/order', function () { return view('app'); });
Route::get('/scout', function () { return view('app'); });
Route::get('/directory', function () { return view('app'); });
Route::get('/friend', function () { return view('app'); });
Route::get('/ranking', function () { return view('app'); });
Route::get('/teamname', function () { return view('app'); });
Route::get('/sns', function () { return view('app'); });
Route::get('/team/{id?}', function () { return view('app'); });
Route::get('/howto', function () { return view('app'); });

Route::get('auth/{driver}/login', 'SnsController@login');
Route::get('auth/{driver}/attach', 'SnsController@attach')->middleware('ballparkuser');
Route::get('auth/{driver}/detach', 'SnsController@detach')->middleware('ballparkuser');
Route::get('auth/{driver}/callback', 'SnsController@callback')->middleware('ballparkuser');
