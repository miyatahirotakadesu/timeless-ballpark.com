<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

class NpbTeam extends Enum
{

    const TEAM_GIANTS = 1;
    const TEAM_TIGERS = 2;
    const TEAM_DRAGONS = 3;
    const TEAM_BAYSTARS = 4;
    const TEAM_CARP = 5;
    const TEAM_SWALLOWS = 6;
    const TEAM_BUFFALOES = 7;
    const TEAM_HAWKS = 8;
    const TEAM_FIGHTERS = 9;
    const TEAM_MARINES = 10;
    const TEAM_LIONS = 11;
    const TEAM_EAGLES = 12;

    const TEAM_PIRATES = 101;
    const TEAM_ROBINS = 102;
    const TEAM_TAKAHASHI_UNIONS = 103;
    const TEAM_DAIEI_UNIONS = 104;
    const TEAM_KINTETSU_BUFFALOES = 105;

    const TEAM_TSUBASAGUN = 201;
    const TEAM_NAGOYAKINKOGUN = 202;
    const TEAM_NISHITETSUGUN = 203;
    const TEAM_YAMATOGUN = 204;

    const TEAM_OTHER = 99999;
    const TEAM_WHITE = 0;

    public static $team_names = [
        self::TEAM_GIANTS => ['読売ジャイアンツ'],
        self::TEAM_TIGERS => ['阪神タイガース'],
        self::TEAM_DRAGONS => ['中日ドラゴンズ'],
        self::TEAM_BAYSTARS => ['横浜DeNAベイスターズ', '横浜ベイスターズ'],
        self::TEAM_CARP => ['広島東洋カープ'],
        self::TEAM_SWALLOWS => ['東京ヤクルトスワローズ'],
        self::TEAM_BUFFALOES => ['オリックス・バファローズ', 'オリックスバファローズ'],
        self::TEAM_HAWKS => ['福岡ソフトバンクホークス'],
        self::TEAM_FIGHTERS => ['北海道日本ハムファイターズ'],
        self::TEAM_MARINES => ['千葉ロッテマリーンズ'],
        self::TEAM_LIONS => ['埼玉西武ライオンズ'],
        self::TEAM_EAGLES => ['東北楽天ゴールデンイーグルス'],

        self::TEAM_PIRATES => ['西日本パイレーツ'],
        self::TEAM_ROBINS => ['松竹ロビンス'],
        self::TEAM_TAKAHASHI_UNIONS => ['高橋ユニオンズ'],
        self::TEAM_DAIEI_UNIONS => ['大映ユニオンズ'],
        self::TEAM_KINTETSU_BUFFALOES => ['大阪近鉄バファローズ'],

        self::TEAM_TSUBASAGUN => ['翼軍'],
        self::TEAM_NAGOYAKINKOGUN => ['名古屋金鯱軍'],
        self::TEAM_NISHITETSUGUN => ['西鉄軍'],
        self::TEAM_YAMATOGUN => ['大和軍'],

        self::TEAM_WHITE => ['WHITE'],
    ];

    public static function getNpbTeamId($teamname)
    {
        foreach (self::$team_names as $npb_team_id => $teamname_arr) {
            if (in_array($teamname, $teamname_arr)) return $npb_team_id;
        }
        return self::TEAM_OTHER;
    }


}


