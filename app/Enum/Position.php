<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

class Position extends Enum
{

    const pitcher = "pitcher";
    const catcher = "catcher";
    const first = "first";
    const second = "second";
    const third = "third";
    const shortstop = "shortstop";
    const left = "left";
    const center = "center";
    const right = "right";
    const designated_hitter = "designated_hitter";

    private static $position_strs = [
        Position::pitcher => '投',
        Position::catcher => '捕',
        Position::first => '一',
        Position::second => '二',
        Position::third => '三',
        Position::shortstop => '遊',
        Position::left => '左',
        Position::center => '中',
        Position::right => '右',
        Position::designated_hitter => '指',
    ];

    public static function getPositionStrs()
    {
        return self::$position_strs;
    }


}

