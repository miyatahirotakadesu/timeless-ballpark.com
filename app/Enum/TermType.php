<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

class TermType extends Enum
{

    const BEFORE_OPENING = 'before_opening';
    const DURING_SEASON = 'during_season';
    const STOVE_LEAGUE = 'stove_league';

}

