<?php

namespace App\Enum;

use MyCLabs\Enum\Enum;

class Role extends Enum
{

    const dependable_pinch_hitter = 'dependable_pinch_hitter';
    const second_pinch_hitter = 'second_pinch_hitter';
    const third_pinch_hitter = 'third_pinch_hitter';
    const utility = 'utility';
    const fielder = 'fielder';
    const starter = 'starter';
    const long_relief = 'long_relief';
    const short_relief = 'short_relief';
    const pre_setup_pitcher = 'pre_setup_pitcher';
    const setup_pitcher = 'setup_pitcher';
    const closer = 'closer';

    private static $batter_roles = [self::dependable_pinch_hitter, self::second_pinch_hitter, self::third_pinch_hitter, self::utility, self::fielder];
    private static $pitcher_roles = [self::starter, self::long_relief, self::short_relief, self::pre_setup_pitcher, self::setup_pitcher, self::closer];

    public static function getBatterRoles()
    {
        return self::$batter_roles;
    }

    public static function getPitcherRoles()
    {
        return self::$pitcher_roles;
    }
    /*
    private static $role_strs = [
        Role::dependable_pinch_hitter => '代打の切り札',
        Role::second_pinch_hitter => '頼れる代打',
        Role::third_pinch_hitter => '代打要因',
        Role::utility => 'ユーティリティ',
        Role::fielder => '守備固め',
        Role::long_relief => 'ロングリリーフ',
        Role::short_relief => 'ワンポイント',
        Role::pre_setup_pitcher => '準セットアッパー',
        Role::setup_pitcher => 'セットアッパー',
        Role::closer => '抑え',
    ];

    private static $role_short_strs = [
        Role::dependable_pinch_hitter => '代打◎',
        Role::second_pinch_hitter => '代打○',
        Role::third_pinch_hitter => '代打',
        Role::utility => '便利屋',
        Role::fielder => '守備',
        Role::long_relief => '長救援',
        Role::short_relief => '短救援',
        Role::pre_setup_pitcher => '７回',
        Role::setup_pitcher => '８回',
        Role::closer => '抑え',
    ];

    public static function getRoleStrs()
    {
        return self::$role_strs;
    }

    public static function getRoleShortStrs()
    {
        return self::$role_short_strs;
    }
    */


}

