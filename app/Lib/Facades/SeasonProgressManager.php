<?php

namespace App\Lib\Facades;

use Illuminate\Support\Facades\Facade;

class SeasonProgressManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'season_progress_manager';
    }
}

