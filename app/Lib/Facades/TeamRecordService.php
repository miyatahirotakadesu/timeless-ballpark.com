<?php

namespace App\Lib\Facades;

use Illuminate\Support\Facades\Facade;

class TeamRecordService extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'team_record_service';
    }
}

