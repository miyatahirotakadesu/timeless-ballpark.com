<?php

namespace App\Lib\Facades;

use Illuminate\Support\Facades\Facade;

class PossessedPlayerManagerService extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'possessed_player_manager_service';
    }
}

