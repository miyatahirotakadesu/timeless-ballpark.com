<?php

namespace App\Lib\Facades;

use Illuminate\Support\Facades\Facade;

class TeamOrderService extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'team_order_service';
    }
}


