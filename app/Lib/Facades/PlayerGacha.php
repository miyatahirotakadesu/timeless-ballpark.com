<?php

namespace App\Lib\Facades;

use Illuminate\Support\Facades\Facade;

class PlayerGacha extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'player_gacha';
    }
}
