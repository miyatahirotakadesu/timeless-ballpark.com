<?php

namespace App\Lib;

use App\Enum\Position as Position;
use App\Enum\Role as Role;

use App\Models\Player as Player;
use App\Models\Team as Team;
use App\Models\TeamOrder as TeamOrder;
use App\Models\PossessedPlayer as PossessedPlayer;

use DB;

class PossessedPlayerManagerService
{
    // 選手の入隊団・引退前後の調節を行うサービス

    private $team;

    public function __construct (Team $team)
    {
        $this->team = $team;
    }

    // チームから選手が引退やトレードによりいなくなってしてしまう前に、その選手がいなくなることでオーダーが崩壊しないかどうか確認する
    // オーダー成立条件：野手は、各ポジションを埋めるスターティングオーダーが成立すること（DH含め最低野手９人）。投手は、先発５人＋中継ぎ４人が存在すること。
    // 全てのTeamOrderがこれを満たすようにする
    public function filterEnableToLeavePlayers(Array $leave_player_ids)
    {
        $unable_to_leave_player_ids = []; // 退団することができない選手リスト

        $possessed_player_ids = $this->team->active_players->pluck('id')->all();

        foreach ($this->team->team_orders as $team_order) {
            $bench_player_ids = $team_order->order_lineup->bench_player_roles->pluck('player_id')->all(); // ベンチ選手IDリスト
            $farm_player_ids = array_diff($possessed_player_ids, $team_order->order_lineup->getLineupPlayerIds());

            $decided_to_substitute_player_ids = []; // 穴埋めによりベンチや二軍からオーダーに入ることが決定する選手のID

            // スタメン野手を順番にみていき引退するかみていく
            foreach ($team_order->order_lineup->starting_order as $order_item) {
                if (in_array($order_item->player_id, $leave_player_ids)) {
                    // スタメン選手が引退する場合

                    $leave_player_position = $order_item->position;

                    // まずベンチを探す
                    $substitutable_player_ids = array_diff($bench_player_ids, $decided_to_substitute_player_ids);
                    $search_players = $this->team->active_players->whereIn('id', $substitutable_player_ids);
                    $substitutable_player_key = $this->getSubstitutableFielderKey($search_players, $leave_player_position);
                    if ($substitutable_player_key !== false) {
                        // ベンチに引退した選手の穴埋めができる選手がいた場合
                        $decided_to_substitute_player_ids[] = $search_players->get($substitutable_player_key)->id;
                        continue;
                    }

                    // いなければ二軍の方も探す
                    $substitutable_player_ids = array_diff($farm_player_ids, $decided_to_substitute_player_ids);
                    $search_players = $this->team->active_players->whereIn('id', $substitutable_player_ids);
                    $substitutable_player_key = $this->getSubstitutableFielderKey($search_players, $leave_player_position);
                    if ($substitutable_player_key !== false) {
                        // 二軍に引退した選手の穴埋めができる選手がいた場合
                        $decided_to_substitute_player_ids[] = $search_players->get($substitutable_player_key)->id;
                        continue;
                    }

                    // 二軍にも代わりとなる選手がいなければ、その選手は退団することができないと確定する
                    $unable_to_leave_player_ids[] = $order_item->player_id;
                }
            }

            // 投手が9人存在する状態を保てるかどうかみていく
            $possessed_pitchers = $this->team->active_players->filter(function($player) {
                $positions = json_decode($player->positions);
                return in_array(Position::pitcher, $positions);
            });
            if (count($possessed_pitchers) < 9) {
                // 投手が9人に満たなくなる場合
                $unableToLeavePlayerCount = 9 - count($possessed_pitchers);
                // 退団予定選手のうち後ろからN人を退団不可とする
                $to_leave_pitchers = $this->team->active_players->filter(function($player) use ($leave_player_ids){
                    $positions = json_decode($player->positions);
                    return in_array($player->id, $leave_player_ids) && in_array(Position::pitcher, $positions);
                })->take(-1 * $unableToLeavePlayerCount);
                foreach ($to_leave_pitchers as $pitcher) {
                    $unable_to_leave_player_ids[] = $pitcher->id;
                }
            }
            
        }

        return $unable_to_leave_player_ids; 
        
    }

    // Playerの配列と守備位置を入力し、最初に発見された守備位置を守ることができる選手のキーを返す。誰もいなければfalseを返す。
    // 守備位置がDHの場合は投手でなければ誰でも良い、として検索する
    private function getSubstitutableFielderKey($search_players, $must_substitute_position)
    {
        return $search_players->search(function($player) use ($must_substitute_position) { 
            $positions = json_decode($player->positions);
            if ($must_substitute_position === Position::designated_hitter) {
                // DHの穴を埋めるのは投手でなければ誰でも良い
                return count(array_diff($positions, [Position::pitcher])) > 0;
            } else {
                return in_array($must_substitute_position, $positions);
            }
        });
    }

    // 試合をするのに最低限必要なメンバーをベンチや二軍から集めてオーダーを作る
    // 選手引退後やトレード実施後に実行される想定
    // 空白オーダーの部分は、player_idがnullになっている
    public function formatTeamStartingOrder()
    {
        foreach ($this->team->team_orders as $team_order) {
            $bench_player_ids = $team_order->order_lineup->bench_player_roles->pluck('player_id')->all(); // ベンチ選手IDリスト
            $decided_to_substitute_player_ids = []; // 穴埋めによりベンチからオーダーに入ることが決定する選手のID

            // スタメン野手
            foreach($team_order->order_lineup->starting_order as &$item) {
                if ($item->player_id === null) {
                    // ベンチから選手を探してスタメンの穴を埋める
                    $substitutable_player_ids = array_diff($bench_player_ids, $decided_to_substitute_player_ids);
                    $search_players = $this->team->players->whereIn('id', $substitutable_player_ids);
                    $substitutable_player_key = $this->getSubstitutableFielderKey($search_players, $item->position);

                    if ($substitutable_player_key === false) throw New \Exception('スタメンを埋める野手がいません');

                    // ベンチの選手をスタメンにする
                    $to_starting_order_player_id = $search_players->get($substitutable_player_key)->id;
                    $decided_to_substitute_player_ids[] = $to_starting_order_player_id;
                    $item->player_id = $to_starting_order_player_id;
                    // スタメンになるため削除するベンチのroleのキー
                    $to_delete_bench_key = $team_order->order_lineup->bench_player_roles->search(function($role_item) use ($to_starting_order_player_id) {
                        return $role_item['player_id'] === $to_starting_order_player_id;
                    });
                    $team_order->order_lineup->bench_player_roles->forget($to_delete_bench_key); // スタメンになったからベンチから追い出す
                }
            }

            // 先発投手
            foreach($team_order->order_lineup->starter_pitchers as &$item) {
                if ($item->player_id === null) {
                    // ベンチから選手を探して先発ローテの穴を埋める
                    $substitutable_player_ids = array_diff($bench_player_ids, $decided_to_substitute_player_ids);
                    $search_players = $this->team->players->whereIn('id', $substitutable_player_ids);
                    $substitutable_player_key = $this->getSubstitutableFielderKey($search_players, Position::pitcher);

                    if ($substitutable_player_key === false) throw New \Exception('ローテを埋める投手がいません');

                    // ベンチの選手を投手ローテ入りさせる
                    $to_starter_pitchers_player_id = $search_players->get($substitutable_player_key)->id;
                    $decided_to_substitute_player_ids[] = $to_starter_pitchers_player_id;
                    $item->player_id = $to_starter_pitchers_player_id;
                    // ローテ入りするため削除するベンチのroleのキー
                    $to_delete_bench_key = $team_order->order_lineup->bench_player_roles->search(function($role_item) use ($to_starter_pitchers_player_id) {
                        return $role_item['player_id'] === $to_starter_pitchers_player_id;
                    });
                    $team_order->order_lineup->bench_player_roles->forget($to_delete_bench_key);  // ローテに入っていったからベンチから追い出す
                }
            }

            // DBに保存
            $team_order_model = $this->team->team_orders->where('id', $team_order['id'])->first();
            $team_order_model->content = $team_order->order_lineup->toJson();
            $team_order_model->save();
        };
    }

    // ベンチにまだ余裕があれば（最大野手７人、投手７人）二軍から集めて余裕を埋めていく
    // 選手引退後やトレード実施後に加え選手獲得後にも実行される想定
    public function formatTeamBench()
    {

        // 引退（退団）する選手は$possessed_playersから除去しておく
        $possessed_player_ids = $this->team->active_players->pluck('id')->all();

        foreach ($this->team->team_orders as $team_order) {
            $lineup_player_ids = $team_order->order_lineup->getLineupPlayerIds();
            $farm_player_ids = array_diff($possessed_player_ids, $lineup_player_ids);

            $batter_roles = Role::getBatterRoles();
            $pitcher_roles = Role::getPitcherRoles();
            $bench_batter_items = $team_order->order_lineup->bench_player_roles->filter(function($item) use ($batter_roles) {
                return in_array($item->role, $batter_roles);
            });
            $bench_pitcher_items = $team_order->order_lineup->bench_player_roles->filter(function($item) use ($pitcher_roles) {
                return in_array($item->role, $pitcher_roles);
            });

            // 野手が７名に満たない場合、二軍からレート高い順にベンチにあげる
            $shortage_batter = TeamOrder::BENCH_BATTER_COUNT - count($bench_batter_items);
            if ($shortage_batter > 0) {
                $farm_batters = $this->team->active_players->whereIn('id', $farm_player_ids)->filter(function($player) {
                    $positions = json_decode($player->positions);
                    return count($positions) > 0 && !in_array(Position::pitcher, $positions);
                })->sortByDesc('rate');
                for ($i = 0; $i < $shortage_batter; $i++) {
                    $farm_batter = $farm_batters->shift();
                    if ($farm_batter === null) break;
                    $team_order->order_lineup->bench_player_roles->push([
                        'role' => Role::utility,
                        'player_id' => $farm_batter->id,
                    ]);
                }
            }
            
            // 投手が７名に満たない場合、二軍からレート高い順にベンチにあげる
            $shortage_pitcher = TeamOrder::BENCH_PITCHER_COUNT - count($bench_pitcher_items);
            if ($shortage_pitcher > 0) {
                $farm_pitchers = $this->team->active_players->whereIn('id', $farm_player_ids)->filter(function($player) {
                    $positions = json_decode($player->positions);
                    return in_array(Position::pitcher, $positions);
                })->sortByDesc('rate');
                for ($i = 0; $i < $shortage_pitcher; $i++) {
                    $farm_pitcher = $farm_pitchers->shift();
                    if ($farm_pitcher === null) break;
                    $team_order->order_lineup->bench_player_roles->push([
                        'role' => Role::long_relief,
                        'player_id' => $farm_pitcher->id,
                    ]);
                }
            }

            // DBに保存
            $team_order_model = $this->team->team_orders->where('id', $team_order['id'])->first();
            $team_order_model->content = $team_order->order_lineup->toJson();
            $team_order_model->save();
        }
    }

    public static function growOld()
    {
        $teams = Team::get();

        $player_columns = ['possessed_players.team_id as team_id', 'players.id as player_id'];
        // 引退年齢になっている間際の所有選手一覧
        $retiring_players = DB::table('possessed_players')
                   ->select($player_columns)
                   ->join('players', 'possessed_players.player_id', '=', 'players.id')
                   ->whereRaw('possessed_players.age = players.retired_age')
                   ->where('possessed_players.retired', 0)
                   ->get();
        \Log::info('retiring_players');
        \Log::info($retiring_players);

        foreach ($teams as $team) {
            $possessed_player_manager_service = new PossessedPlayerManagerService($team);

            // 引退予定選手のうち、ポジションの理由で引退（＝加齢）させちゃダメな選手リスト取得
            $leave_player_ids = $retiring_players->where('team_id', $team->id)->pluck('player_id')->all();
            \Log::info('leave_player_ids');
            \Log::info($leave_player_ids);
            $unable_to_leave_player_ids = $possessed_player_manager_service->filterEnableToLeavePlayers($leave_player_ids);
            \Log::info('unable_to_leave_player_ids');
            \Log::info($unable_to_leave_player_ids);
            // 加齢させちゃダメな選手を除いて、選手に歳をとらせる
            DB::table('possessed_players')
                ->where('team_id', $team->id)
                ->where('retired', 0)
                ->whereNotIn('player_id', $unable_to_leave_player_ids)
                ->whereNotIn('skip_level', [PossessedPlayer::MAX_SKIP_LEVEL])
                ->update(['age' => DB::raw('age + 1')]);

            $possessed_player_manager_service->formatTeamStartingOrder();
            $possessed_player_manager_service->formatTeamBench();
        }

        // 年齢上昇処理後、選手を引退させる
        DB::table('possessed_players')
           ->join('players', 'possessed_players.player_id', '=', 'players.id')
           ->whereRaw('possessed_players.age > players.retired_age')
           ->where('possessed_players.retired', 0)
           ->update(['retired' => true]);
    }

}

