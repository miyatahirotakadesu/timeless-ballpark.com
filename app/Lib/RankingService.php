<?php

namespace App\Lib;

use App\Enum\Position as Position;

use App\Models\Team as Team;
use App\Models\Player as Player;

use DB;

class RankingService
{
    /*
     * [
     *     'team_id' => (int)
     *     'name' => (string)
     *     'rank' => (int)
     *     'win' => (int)
     *     'loss' => (int)
     *     'draw' => (int)
     *     'level' => (int)
     *     'rate' => (int)
     * ]
     */
    public static function updateRanking()
    {
        // 'id' => 'rate' のplayerのrateリストを作成
        $players_arr = Player::select(['id', 'rate'])->get()->toArray();
        $player_rates = [];
        foreach ($players_arr as $player_arr) {
            $player_rates[$player_arr['id']] = $player_arr['rate'];
        }

        $team_ranking_arr = [];
        $teams = Team::with('team_orders')->get()->each(function($team) {
            $team->winning_point = $team->win + $team->draw * 0.001;
            return $team;
        })->sortByDesc('winning_point')->values();

        $now_winning_point = null; // foreachループ内で上書きしながら利用
        $now_rank = null; // foreachループ内で上書きしながら利用
        foreach ($teams as $key => $team) {
            $add_team_arr = []; // これに順位,勝数,負数,レベル,★を入れてから、そのセットを$team_ranking_arrに追加

            $add_team_arr['team_id'] = $team->id;

            // 順位代入
            if ($now_winning_point !== $team->winning_point) {
                $now_winning_point = $team->winning_point;
                $now_rank = $key + 1;
            }
            $add_team_arr['rank'] = $now_rank;

            $add_team_arr['name'] = $team->name;
            $add_team_arr['win'] = $team->win;
            $add_team_arr['lose'] = $team->lose;
            $add_team_arr['draw'] = $team->draw;
            $add_team_arr['level'] = $team->level;

            // オーダーの★合計計算
            $rates = $team->team_orders->map(function($team_order) use ($player_rates) {
                $sum_rate = 0;

                // スタメン野手のrateを足していく
                foreach ($team_order->order_lineup->starting_order as $item) {
                    if ($item->position === Position::pitcher) continue;
                    $sum_rate += $player_rates[$item->player_id];
                }

                // 先発ローテのrateを足していく
                foreach ($team_order->order_lineup->starter_pitchers as $item) {
                    $sum_rate += $player_rates[$item->player_id];
                }

                return $sum_rate;
            })->all();
            $add_team_arr['rate'] = round(array_sum($rates) / count($rates));

            $team_ranking_arr[] = $add_team_arr;
        }

        $ranking_filename = storage_path(env('RANKING_FILENAME'));
        file_put_contents($ranking_filename, json_encode($team_ranking_arr, JSON_UNESCAPED_UNICODE));
    }

    /*
     * ランキング情報を配列で返す
     */
    public static function getRanking()
    {
        $ranking_filename = storage_path(env('RANKING_FILENAME'));
        $ranking_json = file_get_contents($ranking_filename);
        $ranking_arr = json_decode($ranking_json, true);
        return $ranking_arr;
    }

    /*
     * ファイルに保存されているランキング情報とDBのteamsテーブルのチーム順位カラムを同期させる
     */
    public static function syncTeamToRanking()
    {
        $ranking = self::getRanking();
        $queries = '';

        $team = new Team();
        $tablename = $team->getTable();

        foreach ($ranking as $item) {
            $queries .= 'UPDATE ' . $tablename . ' SET rank = ' . $item['rank'] . ' WHERE id = '. $item['team_id'] . ';';
        }
        DB::raw($queries);

    }
}

