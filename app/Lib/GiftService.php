<?php

namespace App\Lib;

use App\Models\Mail as Mail;
use App\Models\Team as Team;
use App\Models\SystemParam as SystemParam;

class GiftService
{

    const INTRODUCING_TYPE_ACCESS = 10; // サイト訪問報酬
    const INTRODUCING_TYPE_REGIST = 20; // ゲーム登録報酬
    const INTRODUCING_TYPE_SCOUT = 30;  // 20人スカウト報酬
    private static $INTRODUCING_REWARD_COINS = [
        self::INTRODUCING_TYPE_ACCESS => 20,
        self::INTRODUCING_TYPE_REGIST => 1000,
        self::INTRODUCING_TYPE_SCOUT  => 2000
    ];

    const INTRODUCING_SCOUT_REWARD_PLAYER_COUNT = 38;  // 初期メンバー18人 + 20回スカウト実行したら、紹介者に報酬

    public static function receive(Team $team, Mail $mail)
    {
        if ($mail->received !== 0) throw new \RuntimeException('既にギフトを受け取っています。'); // 受け取り済みギフトの場合処理を抜ける
        if ($mail->team_id !== $team->id)  throw new \LogicException('メール送信先チームとギフト受け取りチームが一致しません。'); // 該当チームに対するギフトではない場合処理を抜ける

        switch ($mail->gift_type) {
            case Mail::GIFT_TYPE_COIN:
                if (!is_numeric($mail->gift_value)) throw new \LogicException('受取コインの値が数値ではありません。');
                $team->coin += (int)$mail->gift_value;
                $mail->received = true;
                $team->save();
                $mail->save();
                break;
        }

        return true;
    }

    public static function giftByEndingSeason()
    {
        $mail_title_template = '第%season%シーズンの全日程が終了しました';
        $mail_body_template = "%team_name%の成績は%win%勝%lose%敗%draw%分で、%team_count%チーム中%rank%位でした。\n賞金として%coin%COINをプレゼントします！";

        $season = SystemParam::getSystemParam(SystemParam::KEY_SEASON);
        $ranking = collect(RankingService::getRanking());
        $team_count = $ranking->count();

        $ranking->map(function($ranking_item) use ($mail_title_template, $mail_body_template, $team_count, $season) {
            $coin = self::calcEndingSeasonCoin($ranking_item['rank'], $team_count, $ranking_item['win']);
            $mail = new Mail();
            $mail->team_id = $ranking_item['team_id'];
            $mail->read_flag = false;
            $mail->title = str_replace(['%season%'], [$season], $mail_title_template);
            $mail->body = str_replace(['%team_name%', '%win%', '%lose%', '%draw%', '%team_count%', '%rank%', '%coin%'],
                [$ranking_item['name'], $ranking_item['win'], $ranking_item['lose'], $ranking_item['draw'], $team_count, $ranking_item['rank'], $coin], $mail_body_template);
            $mail->gift_type = Mail::GIFT_TYPE_COIN;
            $mail->gift_value = $coin;
            $mail->received = false;

            $mail->save();
        });
    }

    /*
     * シーズン終了ボーナスとしていくらCOINをプレゼントするか計算する
     */
    private static function calcEndingSeasonCoin($rank, $team_count, $win)
    {
        $coin = 1000;
        // 上位何％に入っているかボーナス
        $rank_rate = $rank / $team_count; 
        switch (true) {
            case $rank_rate < 0.1:
                $coin += 10000;break;
            case $rank_rate < 0.3:
                $coin += 8000;break;
            case $rank_rate < 0.5:
                $coin += 5000;break;
           default :
                break;
        }

        // キリのいい順位ならボーナス
        if ($rank % 10 === 0) $coin += 500;
        if ($rank % 100 === 0) $coin += 3000;
        if ($rank % 1000 === 0) $coin += 10000;
        if ($rank % 10000 === 0) $coin += 50000;

        // 勝利数に応じたボーナス
        $coin += 100 * $win;

        return $coin;
    }

    /*
     * 紹介コードを含んだURLへのアクセス/登録/継続遊戯(20スカウト)による報酬
     */
    public static function rewardByIntroducing(Team $team, $introducing_type, $introduced_team_name = null)
    {
        if (!in_array($introducing_type, array_keys(self::$INTRODUCING_REWARD_COINS))) throw \LogicException('不正な紹介報酬タイプです。');

        $coin = self::$INTRODUCING_REWARD_COINS[$introducing_type];

        switch ($introducing_type) {
            case self::INTRODUCING_TYPE_ACCESS:
                $mail_title_template = 'シェア報酬COINのプレゼント';
                $mail_body_template = "あなたがシェアしたURLがクリックされました！\n報酬として" . $coin . "COINをプレゼントします。";
                break;
            case self::INTRODUCING_TYPE_REGIST:
                $mail_title_template = '友達紹介報酬COINのプレゼント';
                $mail_body_template = "あなたの紹介により、" . $introduced_team_name . "がゲームを始めました！\n報酬として" . $coin . "COINをプレゼントします！";
                break;
            case self::INTRODUCING_TYPE_SCOUT:
                $mail_title_template = '友達紹介報酬COINのプレゼント';
                $mail_body_template = "あなたの紹介によりゲームで遊んでいる" . $introduced_team_name . "が、合計２０選手をスカウトしました！\n報酬として" . $coin . "COINをプレゼントします！";
                break;
        }

        $mail = new Mail();
        $mail->team_id = $team->id;
        $mail->read_flag = false;
        $mail->title = $mail_title_template;
        $mail->body = $mail_body_template;
        $mail->gift_type = Mail::GIFT_TYPE_COIN;
        $mail->gift_value = $coin;
        $mail->received = false;

        $mail->save();
    }

}

