<?php

namespace App\Lib;

use App\Enum\Position as Position;

use App\Models\SystemParam as SystemParam;
use App\Models\Game as Game;
use App\Models\Team as Team;
use App\Models\TeamOrder as TeamOrder;
use App\Models\Columns\OrderLineup as OrderLineup;

class TeamOrderService
{
    /*
     * オーダーに問題がないかチェックする
     * 試合前のオーダーチェックに利用する時は第２、第３引数はNULLのまま
     * 更新前のバリデーションチェックに利用する時は第２、第３引数に値が入る
     */
    public static function validateOrRepair(Team $team, $updating_order_type = null, OrderLineup $new_order_lineup = null)
    {
        $active_players = $team->active_players;

        try {
            foreach ($team->team_orders as $team_order) {
                $order_lineup = new OrderLineup($team_order->content);
                if ($team_order->order_type === $updating_order_type) {
                    $order_lineup = $new_order_lineup;
                }

                if (!$order_lineup->validate()) throw new \RuntimeException('不正なオーダーです');
                if (!empty(array_diff($order_lineup->getLineupPlayerIds(), $active_players->pluck('id')->all()))) throw new \RuntimeException('保有していないまたは現役選手ではない選手がオーダーに含まれています');
                // 守備位置問題ないかチェック
                foreach ($order_lineup->starting_order as $item) {
                    $player = $active_players->where('id', $item->player_id)->first();
                    $positions = json_decode($player->positions);
                    if ($item->position !== Position::designated_hitter && !in_array($item->position, $positions)) throw new \RuntimeException('守備位置が不適切な選手が存在します');
                }
                // 先発投手制限に引っかからないかチェック
                $starter_restriction_map = self::makeStarterRestrictionMap($team->id);
                foreach ($order_lineup->starter_pitchers as $item) {
                    $order_num = $item->order; // N番手
                    // N番手にplayer_idは入っちゃダメって$starter_restriction_mapに書いてるのに入ってる場合はException.
                    if (in_array($item->player_id, $starter_restriction_map['order_' . $order_num])) throw new \RuntimeException('先発投手の登板間隔が短くなってしまうため設定できません');

                }
                foreach ($order_lineup->bench_player_roles->pluck('player_id')->all() as $bench_player_id) {
                    if (in_array($bench_player_id, $starter_restriction_map['bench'])) throw new \RuntimeException('最近4試合以内に先発した投手はベンチ入りすることはできません。');
                }

            }
        } catch(\Exception $e) {
            self::repairOrder($team);
        } 

    }

    /*
     * オーダーを自動修復する
     */
    private static function repairOrder(Team $team)
    {

        // スタメン９人と先発ローテ５人を★の高い順に探して埋める
        $team_order_arr = ['starting_order' => [], 'starter_pitchers' => [], 'bench_player_roles' => []]; // ガチャ実行と並行してオーダー情報も構築していく

        // スタメン９人
        $position_arr = [Position::center,  Position::second, Position::first, Position::third,
                         Position::left, Position::shortstop, Position::right, Position::catcher, Position::designated_hitter]; // ''は特に指定がないことを表す（LIKE検索でそのまま使うため)
        $batting_order_cnt = 1;
        $listed_player_ids = [];
/*
	$player_counts_by_position = collect(array_map(function($position) use ($team) {
	    return [
	    	'position' => $position,
		'count' => $team->active_players->filter(function($player) use ($position) {
                    return in_array($position, json_decode($player->positions));
		})->count()
	    ];
	}, $position_arr))->min('count')->values();
*/
        foreach ($position_arr as $position) {
            $player = $team->active_players->whereNotIn('id', $listed_player_ids)->filter(function($player) use ($position) {
                if (in_array(Position::pitcher, json_decode($player->positions))) return false;
                if ($position === Position::designated_hitter) return true;
                return in_array($position, json_decode($player->positions));
            })->sortByDesc('rate')->first();

if ($player === null) {
    $player = $team->active_players->whereNotIn('id', $listed_player_ids)->sortByDesc('rate')->first();
}

            // 6番指名打者、9番ショートにしたいから。でも指名打者よりショートを優先的に探さなければならないのでここで泥臭く入れ替える
            if ($batting_order_cnt === 6) {
                $batting_order = 9;
            } else if ($batting_order_cnt === 9) {
                $batting_order = 6;
            } else {
                $batting_order = $batting_order_cnt;
            }

            // batting_order用配列に追加
            $team_order_arr['starting_order'][] = [
                'batting_order' => $batting_order,
                'position' => $position,
                'player_id' => $player->id
            ];
            $listed_player_ids[] = $player->id;
            $batting_order_cnt++;
        }
        $team_order_arr['starting_order'] = collect($team_order_arr['starting_order'])->sortBy('batting_order')->values()->toArray(); // 6番と9番を整えるため

        $pitchers = $team->players->filter(function($player) use ($position) {
            return in_array(Position::pitcher, json_decode($player->positions));
        })->sortByDesc('rate')->take(5);
        $pitcher_cnt = 1;
        foreach ($pitchers as $player) {
            $team_order_arr['starter_pitchers'][] = [
                'order' => $pitcher_cnt,
                'player_id' => $player->id
            ];
            $pitcher_cnt++;
        }

        $team_order = $team->team_orders->where('order_type', TeamOrder::ORDER_TYPE_WITH_DH)->first();
        $team_order->content = json_encode($team_order_arr);
        $team_order->save();

        // ベンチはPossessedPlayerManagerServiceで埋める
        $possessed_player_manager_service = new PossessedPlayerManagerService($team);
        $possessed_player_manager_service->formatTeamBench();
    }

    /*
     * [ 'order_1' => [50, 40, 30, 20], 'order_2' => [40,30,20], 'order_3' => [30,20], 'order_4' => [20], 'order_5' => [], 'bench' => [50, 40, 30, 20] ]
     * のような、'order_N番手' => [先発不可player_idリスト] を作成し返す
     * 最近4試合の先発投手に縛りをかける
     *
     * 【ソースわかりずらいので具体例】
     * 今のgame_time = 22 とする
     * 最近５試合の先発投手が
     * game_time:18 つまり22まで投げれないのが player_id=10
     * game_time:19 つまり23まで投げれないのが player_id=20
     * game_time:20 つまり24まで投げれないのが player_id=30
     * game_time:21 つまり25まで投げれないのが player_id=40
     * game_time:22 つまり26まで投げれないのが player_id=50
     *
     * 23試合目 = order_3に先発不可のplayer_id達 [20,30,40,50]
     * 24試合目 = order_4に先発不可のplayer_id達 [30,40,50]
     * 25試合目 = order_5に先発不可のplayer_id達 [40,50]
     * 26試合目 = order_1に先発不可のplayer_id達 [50]
     * 27試合目 = order_2に先発不可のplayer_id達 誰でも入れる
     *
     */
    public static function makeStarterRestrictionMap($team_id)
    {
        $starter_restriction_map = ['order_1' => [], 'order_2' => [], 'order_3' => [], 'order_4' => [], 'order_5' => [], 'bench' => []];

        $game_time = (int)SystemParam::getSystemParam(SystemParam::KEY_GAME_TIME);
        // 最近4試合のゲーム取得
        $games = Game::select(['game_number', 'home_team_id', 'visitor_team_id', 'home_team_starter_player_id', 'visitor_team_starter_player_id'])
                     ->where('game_number', '<', $game_time)
                     ->where('game_number', '>', ($game_time - 5))
                     ->where('executed', true)->where(function($q) use ($team_id) { $q->where('home_team_id', $team_id)->orWhere('visitor_team_id', $team_id); })
                     ->get();
        // ['game_count' => 'starter_player_id']の配列（最大長さ４） game_count試合目の先発投手はstarter_player_idだったという意味
        $game_starter_map = [];
        $games->each(function($game) use (&$game_starter_map, $team_id) {
            $game_number = $game->game_number;
            $game_starter_map[$game_number] = ($game->home_team_id === $team_id) ? $game->home_team_starter_player_id : $game->visitor_team_starter_player_id;
        });

        foreach ($games as $game) {
            $game_prev_count = $game_time - $game->game_number; // 1~4の値。N試合前の試合、という意味の値

            // 1試合前に投げた選手は次の(5-1)=4試合に登板不可、3試合前に投げた選手は次の(5-3)=2試合に登板不可能
            // 1試合前に投げた選手は$starter_restriction_mapの4箇所に名を連ねる
            // 3試合前に投げた選手は$starter_restriction_mapの2箇所に名を連ねる
            for ($i = 0; $i < 5 - $game_prev_count; $i++) {
                $order_num = ($game_time + $i) % 5 + 1; // 1~5
                $starter_restriction_map['order_' . $order_num][] = $game_starter_map[$game->game_number]; // 先発不可リストにデータ追加
            }
        }

        // 最近4試合に登板した先発投手は全員benchに入ることも許可しない
        $next_game_order_num = $game_time % 5 + 1; // 1~5
        $starter_restriction_map['bench'] = $starter_restriction_map['order_' . $next_game_order_num];

        return $starter_restriction_map;
    }


}


