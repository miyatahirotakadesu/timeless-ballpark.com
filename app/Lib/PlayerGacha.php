<?php

namespace App\Lib;

use App\Enum\Position as Position;
use App\Enum\Role as Role;

use App\Models\Player as Player;
use App\Models\Team as Team;
use App\Models\TeamOrder as TeamOrder;
use App\Models\PossessedPlayer as PossessedPlayer;

use App\Lib\GiftService as GiftService;

class PlayerGacha
{

    // チーム初期メンバー決定ガチャ
    /*
     * 野手 9人 ★6, 5, 4, 4, 3, 3, 2, 2, 1 各ポジション8人+ポジション関係なく1人
     * 投手 9人 ★6, 5, 4, 4, 3, 3, 2, 2, 1
     *
     * この関数の外側でトランザクションがなされている想定のため、この関数内ではトランザクションをかけない
     */
    public static function makeTeamGacha(Team $team)
    {
        $rate_rnd_arr = [8, 8, 7, 6, 5, 4, 3, 2, 1];
        shuffle($rate_rnd_arr);
        $position_arr = [Position::center,  Position::second, Position::first, Position::third,
                         Position::left, Position::designated_hitter, Position::right, Position::catcher,  Position::shortstop]; // ''は特に指定がないことを表す（LIKE検索でそのまま使うため)
        $fielder_gacha_map = array_combine($position_arr, $rate_rnd_arr);

        $acquired_player_ids = []; // 重複防止のため
        $team_order_arr = ['starting_order' => [], 'starter_pitchers' => [], 'bench_player_roles' => []]; // ガチャ実行と並行してオーダー情報も構築していく

        // 野手9人のガチャを実行
        $batting_order_cnt = 1;
        foreach ($fielder_gacha_map as $position => $rate) {
            if ($position === Position::designated_hitter) {
                // 野手なら誰でも
                $player = Player::where('positions', 'not like', '%"pitcher"%')->where('joined_year', '>', 2000)->where('rate', $rate)->whereNotIn('id', $acquired_player_ids)->get()->random();
            } else {
                // 指定ポジションを守れる選手
                $position_like = '%"' . $position . '"%';
                $where_year = ($position === Position::catcher) ? 1970 : 2000;
                $player = Player::where('positions', 'like', $position_like)->where('joined_year', '>', $where_year)->where('rate', $rate)->whereNotIn('id', $acquired_player_ids)->get()->random();
            }
            $acquired_player_ids[] = $player->id;

            $age = mt_rand($player->joined_age, $player->retired_age); // 年齢決定
            $team->players()->attach($player->id, ['team_id' => $team->id, 'age' => $age]); // 多対多テーブルにINSERTし選手獲得

            // batting_order用配列に追加
            $team_order_arr['starting_order'][] = [
                'batting_order' => $batting_order_cnt,
                'position' => $position,
                'player_id' => $player->id
            ];
            $batting_order_cnt++;
        }

        // 投手9人のガチャを実行
        shuffle($rate_rnd_arr);
        $pitcher_cnt = 0;
        foreach ($rate_rnd_arr as $rate) {
            $player = Player::where('positions', 'like', '%"pitcher"%')->where('joined_year', '>', 2000)->where('rate', $rate)->whereNotIn('id', $acquired_player_ids)->get()->random();
            $acquired_player_ids[] = $player->id;

            $age = mt_rand($player->joined_age, $player->retired_age); // 年齢決定
            $team->players()->attach($player->id, ['team_id' => $team->id, 'age' => $age]); // 多対多テーブルにINSERTし選手獲得

            // batting_order用配列に追加
            if ($pitcher_cnt < 5) {
                $team_order_arr['starter_pitchers'][] = [
                    'order' => $pitcher_cnt + 1,
                    'player_id' => $player->id
                ];
            } else {
                switch (true) {
                    case $pitcher_cnt < 6:
                        $role = Role::long_relief;break;
                    case $pitcher_cnt < 7:
                        $role = Role::pre_setup_pitcher;break;
                    case $pitcher_cnt < 8:
                        $role = Role::setup_pitcher;break;
                    case $pitcher_cnt < 9:
                        $role = Role::closer;break;
                    default :
                        $role = Role::short_relief;break; // 投手は9人なのでここは通らないはず
                }
                $team_order_arr['bench_player_roles'][] = [
                    'role' => $role,
                    'player_id' => $player->id
                ];
            }
            $pitcher_cnt++;
        }

        // $team_order_arrからTeamOrder生成していく
        // DHなしのオーダー作成
        /*
        $team_order = new TeamOrder();
        $team_order->team_id = $team->id;
        $team_order->order_type = TeamOrder::ORDER_TYPE_WITHOUT_DH;
        $content_without_dh = $team_order_arr;
        // 9番打者にいる野手をベンチに、代わりに9番打者は投手とするよう整形
        $content_without_dh['bench_player_roles'][] = [
            'role' => Role::dependable_pinch_hitter,
            'player_id' => $content_without_dh['starting_order'][8]['player_id']
        ];
        $content_without_dh['starting_order'][8] = [
            'batting_order' => 9,
            'position' => Position::pitcher
        ];
        $team_order->content = json_encode($content_without_dh);
        $team_order->save();
         */
        
        // DHありのオーダー作成
        $team_order = new TeamOrder();
        $team_order->team_id = $team->id;
        $team_order->order_type = TeamOrder::ORDER_TYPE_WITH_DH;
        //$content_without_dh = $team_order_arr;
        // 9番打者のポジションをDHに設定
        //$content_without_dh['starting_order'][8]['position'] = Position::designated_hitter;
        $team_order->content = json_encode($team_order_arr);
        $team_order->save();
        
    }

    // 通常ガチャ実行
    public static function execGacha(Team $team, $times)
    {
        $result_collection = collect([]); 
        $acquired_players = Player::get()->random($times);

        if ($team->user !== null && $team->user->user_id_introduced_by !== null) {
            $exist_player_count = $team->players->count();
            if ($this->user->hasSnsAuth && $exist_player_count < GiftService::INTRODUCING_SCOUT_REWARD_PLAYER_COUNT && $exist_player_count + $times >= GiftService::INTRODUCING_SCOUT_REWARD_PLAYER_COUNT) {
                // SNS連携しているユーザーが、ガチャをひいたことで報酬規定人数に達する場合、紹介者に報酬プレゼント
                $introducing_user = User::where('id', $team->user->user_id_introduced_by)->firstOrFail();
                if ($introducing_user->team !== null) GiftService::rewardByIntroducing($introducing_user->team, GiftService::INTRODUCING_TYPE_SCOUT);
            }
        }

        foreach($acquired_players as $acquired_player) {
            $exist_player = $team->players->where('id', $acquired_player->id)->first();
            if ($exist_player === null) {
                // 持っていない選手を獲得
                $age = mt_rand($acquired_player->joined_age, $acquired_player->retired_age); // 年齢決定
                $team->players()->attach($acquired_player->id, ['team_id' => $team->id, 'age' => $age]); // 多対多テーブルにINSERTし選手獲得
            } else {
                // 持っている選手を獲得
                $exist_player->pivot->skip_level = $exist_player->pivot->skip_level + 1;
                if ($exist_player->pivot->skip_level > PossessedPlayer::MAX_SKIP_LEVEL) $exist_player->pivot->skip_level = PossessedPlayer::MAX_SKIP_LEVEL;
                $exist_player->pivot->save();
            }

            $result_collection->push($team->players()->where($acquired_player->getTable() . '.id', $acquired_player->id)->firstOrFail());
        }

        return $result_collection;
    }
}

