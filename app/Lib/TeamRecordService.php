<?php

namespace App\Lib;

use App\Models\Team as Team;
use App\Models\TeamRecord as TeamRecord;
use App\Models\SystemParam as SystemParam;

use RankingService;

class TeamRecordService
{
    /*
     * シーズン終了時に呼び出される想定
     * teamの勝敗数、順位、オーダーをteam_recordsに保存していく
     */
    public static function record()
    {
        $season = SystemParam::getSystemParam(SystemParam::KEY_SEASON);

        $ranking = collect(RankingService::getRanking());
        $team_count = $ranking->count();
        $team_records = $ranking->map(function($ranking_item) use ($season, $team_count) {
            $team = Team::with('team_orders')->where('id', $ranking_item['team_id'])->firstOrFail();

            $team_orders_arr = [];
            $team->team_orders->each(function($team_order) use (&$team_orders_arr) {
                $team_orders_arr[$team_order->order_type] = $team_order->order_lineup->toArray();
            });

            return [
                'team_id' => $ranking_item['team_id'],
                'season' => $season,
                'rank' => $ranking_item['rank'],
                'team_count' => $team_count,
                'team_orders' => json_encode($team_orders_arr, JSON_UNESCAPED_UNICODE),
                'win' => $team->win,
                'lose' => $team->lose,
                'draw' => $team->draw,
                'level' => $team->level,
            ];
        })->toArray();

        return TeamRecord::insert($team_records);
    }
}

