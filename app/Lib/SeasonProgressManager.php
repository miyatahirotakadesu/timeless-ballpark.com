<?php

namespace App\Lib;

use App\Enum\TermType as TermType;

use App\Models\Game as Game;
use App\Models\Team as Team;
use App\Models\TeamResult as TeamResult;
use App\Models\SystemParam as SystemParam;
use App\Models\PossessedPlayer as PossessedPlayer;

use DB;

use GameService;
use GameTableService;
use RankingService;
use TeamRecordService;
use PossessedPlayerManagerService;

class SeasonProgressManager
{
    // ゲームの進行管理をする

    const GAME_TIME_COUNT_PER_SEASON = 150; // 1シーズンの試合数
    const GAME_TIME_COUNT_STOVE_LEAGUE = 151;

    /*
     * 現在のgame_timeを取得し、その値に応じたterm_typeを取得する
     * 次にterm_typeに応じた処理を実行する
     * 最後にgame_timeを1進め、もしシーズン終了であればリセットも行う
     */
    public static function progress()
    {
        $game_time = (int)SystemParam::getSystemParam(SystemParam::KEY_GAME_TIME);
        $term_type = self::toTermType($game_time);

        switch($term_type) {
            case TermType::BEFORE_OPENING:
                // 前シーズンの成績削除
                Game::truncate();
                // ユーザー登録に至っていない（＝userと紐づいていない＆登録日が5日前以上）チーム削除
                $teams = Team::where('user_id', null)->whereDate('created_at', '<', date('Y-m-d', strtotime('-5 day')))->get();
                $teams->each(function($team) { $team->delete(); }); // カスケード削除のために全件一括削除ではなく１つずつ削除
                // 全てのチームの成績リセット
                $team = new Team();
                DB::table($team->getTable())->update([
                    'changed_name' => false,
                    'win' => 0,
                    'lose' => 0,
                    'draw' => 0,
                    'consecutive_wins' => 0,
                ]);
                // 試合日程作成
                GameTableService::makeGameTable($game_time + 1);

                break;
            case TermType::DURING_SEASON:
                // 予定されている試合の全て実行
                GameService::processGames($game_time); 
                // ランキング更新、チーム順位情報更新
                RankingService::updateRanking();
                RankingService::syncTeamToRanking();
                // （次のgame_timeもシーズン中であれば）試合日程作成
                if ($game_time + 1 <= self::GAME_TIME_COUNT_PER_SEASON) GameTableService::makeGameTable($game_time + 1);

                break;
            case TermType::STOVE_LEAGUE:
                // team_resultsテーブルに各チームのシーズン成績を記録
                TeamRecordService::record();
                // シーズン終了に伴うギフト配布
                GiftService::giftByEndingSeason();
                // シーズン終了に伴う加齢処理
                PossessedPlayerManagerService::growOld();

                break;
            default :
                throw new \LogicException('不正なterm_type値です');
        }

        // ゲーム内時間を進行させる
        $game_time++;
        if ($term_type === TermType::STOVE_LEAGUE) {
            $season = SystemParam::getSystemParam(SystemParam::KEY_SEASON);
            $season++;
            SystemParam::setSystemParam(SystemParam::KEY_SEASON, $season);
            $game_time = 0;
        }
        SystemParam::setSystemParam(SystemParam::KEY_GAME_TIME, $game_time);

    }

    private static function toTermType($game_time)
    {
        if ($game_time === 0) {
            return TermType::BEFORE_OPENING;
        } else if ($game_time <= self::GAME_TIME_COUNT_PER_SEASON) {
            return TermType::DURING_SEASON;
        } else if ($game_time === self::GAME_TIME_COUNT_STOVE_LEAGUE)  {
            return TermType::STOVE_LEAGUE;
        } else {
            throw new \LogicException('不正なgametime値です');
        }
    }

}

