<?php

namespace App\Lib;

use App\Enum\Position as Position;
use App\Enum\Role as Role;

use App\Models\Game as Game;
use App\Models\Team as Team;
use App\Models\TeamOrder as TeamOrder;
use App\Models\Player as Player;

use DB;
use TeamOrderService;

class GameService
{

    const COIN_BASE_WIN_TEAM  = 10;
    const COIN_BASE_LOSS_TEAM = 5;
    const COIN_BASE_DRAW_TEAM = 7;

    const GAME_RESULT_WIN = 'win';
    const GAME_RESULT_LOSE = 'lose';
    const GAME_RESULT_DRAW = 'draw';

    public static function processGames($game_time)
    {
        $games = Game::where('game_number', $game_time)->where('executed', false)->get();
        $teams = Team::get();
        // 試合前にオーダーが壊れていないかチェック
        foreach ($teams as $team) {
            TeamOrderService::validateOrRepair($team);
        }
        $teams = Team::with('team_orders')->get();
        $is_dh_game = $game_time % 2 === 0; // 偶数試合目はDHありの試合
        $starter_order = ($game_time % 5) + 1;
        foreach ($games as $game) {

            $home_team = $teams->where('id', $game->home_team_id)->first();
            $visitor_team = ($game->visitor_team_id === -1) ? 'substitute_team' : $teams->where('id', $game->visitor_team_id)->first();

            $order_array = [
                'home' => self::makeOrderArray($home_team, $is_dh_game, $starter_order),
                'visitor' => ($visitor_team === 'substitute_team') ? self::makeSubstituteTeamOrderArray($is_dh_game) : self::makeOrderArray($visitor_team, $is_dh_game, $starter_order),
            ];
            $home_pitcher_batting_order = 'order' . $order_array['home']['positions'][Position::pitcher];
            $game->home_team_starter_player_id = $order_array['home']['order'][$home_pitcher_batting_order];
            $visitor_pitcher_batting_order = 'order' . $order_array['visitor']['positions'][Position::pitcher];
            $game->visitor_team_starter_player_id = $order_array['visitor']['order'][$visitor_pitcher_batting_order];

            /* ユニークなファイル名の一時ファイルを作成 */
            $tmpfname = tempnam("/tmp", "timeless_ballpark-");

            $handle = fopen($tmpfname, "w");
            chmod($tmpfname, 0777);
            fwrite($handle, json_encode($order_array, JSON_UNESCAPED_UNICODE));
            fclose($handle);

            $result_json = null;
            exec('export LANG=ja_JP.UTF-8;' . env('BASEBALLSIMULATOR_PASS') . ' ' . $tmpfname, $result_json, $result);
            $content = rtrim($result_json[0]);
            $content_arr = json_decode($content, true);

            unlink($tmpfname);

            // サヨナラ勝ちかどうか判定
            $home_last_scoreboard = (array_slice($content_arr['scoreHighLight']['home'], -1, 1, true));
            $last_inning = key($home_last_scoreboard);
            $last_inning_score = current($home_last_scoreboard);
            // ホームチームが勝ち＆最後のホームの攻撃が９回以降＆その最終回に点を取っている＝サヨナラゲーム
            $walkoff_game_flg = $content_arr['score']['home'] > $content_arr['score']['visitor'] && $last_inning >= 9 && $last_inning_score > 0;

            $game->executed = true;
            $game->home_team_score = $content_arr['score']['home'];
            $game->visitor_team_score = $content_arr['score']['visitor'];
            $game->walkoff_game_flg = $walkoff_game_flg;
            $game->content = $content;
            $game->save();

            // 勝敗記録、COIN付与
            if ($content_arr['score']['home'] === $content_arr['score']['visitor']) { // 引き分け
                self::updateTeamsByGameset($home_team, self::GAME_RESULT_DRAW);
                if ($visitor_team !== 'substitute_team') self::updateTeamsByGameset($visitor_team, self::GAME_RESULT_DRAW);
            } else if ($content_arr['score']['home'] > $content_arr['score']['visitor']) { // ホームの勝ち
                self::updateTeamsByGameset($home_team, self::GAME_RESULT_WIN);
                if ($visitor_team !== 'substitute_team') self::updateTeamsByGameset($visitor_team, self::GAME_RESULT_LOSE);
            } else { // ビジターの勝ち
                self::updateTeamsByGameset($home_team, self::GAME_RESULT_LOSE);
                if ($visitor_team !== 'substitute_team') self::updateTeamsByGameset($visitor_team, self::GAME_RESULT_WIN);
            }

        }
    }

    /*
     * 試合を行った両チームに勝敗記録とCOIN付与を行う
     */
    private static function updateTeamsByGameset(Team $team, $result)
    {
        switch ($result) {
            case self::GAME_RESULT_WIN;
                $team->coin += self::COIN_BASE_WIN_TEAM + round($team->level * 0.1);
                $team->win++;
                $team->consecutive_wins++;
                $team->exp++;
                $team->updateLevel();
                $team->total_win++;
                break;
            case self::GAME_RESULT_LOSE:
                $team->coin += self::COIN_BASE_LOSS_TEAM + round($team->level * 0.05);
                $team->lose++;
                $team->consecutive_wins = 0;
                $team->total_lose++;
                break;
            case self::GAME_RESULT_DRAW:
                $team->coin += self::COIN_BASE_DRAW_TEAM + round($team->level * 0.07);
                $team->draw++;
                $team->total_draw++;
                break;
        }
        $team->updateLevel();
        $team->save();
    }

    /*
     * C#プログラムに試合させるための入力JSONを作るための関数
     */
    private static function makeOrderArray(Team $team, $is_dh_game, $starter_order)
    {
        $team_order = $team->team_orders->where('order_type', TeamOrder::ORDER_TYPE_WITH_DH)->first();

        $order = [];
        $positions = [];
        $bench_player_ids = [];
        $players = [];

        $starter_player_key = $team_order->order_lineup->starter_pitchers->search(function($item) use ($starter_order) { return $item->order === $starter_order; });
        $starter_player_id = $team_order->order_lineup->starter_pitchers->get($starter_player_key)->player_id;

        if ($is_dh_game === true) {
            $team_order->order_lineup->starting_order->push((object)[
                'batting_order' => 0,
                'position' => Position::pitcher
            ]);
        } else {
            $team_order->order_lineup->toWithoutDHOrder();
        }

        // $orderと$positionsにデータ入れていく
        foreach ($team_order->order_lineup->starting_order as $item) {
            $order_key = 'order' . $item->batting_order;
            $order[$order_key] = ($item->position === Position::pitcher) ? $starter_player_id : $item->player_id;

            $positions_key = $item->position;
            $positions[$positions_key] = $item->batting_order;
        }
        
        // $bench_player_idsにデータ入れていく
        foreach ($team_order->order_lineup->bench_player_roles as $item) {
            $bench_player_id = $item->player_id;
            $bench_player_ids[$bench_player_id] = $item->role;
        }

        // $playersを作っていく
        $player_selects = ['players.id as player_id', 'players.name as player_name', 'players.positions as player_positions'];
        $batter_selects = ['player_batting_results.year as batting_result_year', 'player_batting_results.game as batting_result_game', 'player_batting_results.plate_appearance as batting_result_plate_appearance', 'player_batting_results.hits as batting_result_hits', 'player_batting_results.twobase as batting_result_twobase', 'player_batting_results.triple as batting_result_triple', 'player_batting_results.homerun as batting_result_homerun', 'player_batting_results.rbi as batting_result_rbi', 'player_batting_results.fourball as batting_result_fourball', 'player_batting_results.deadball as batting_result_deadball', 'player_batting_results.strikeout as batting_result_strikeout', 'player_batting_results.sacrifice_bunt as batting_result_sacrifice_bunt', 'player_batting_results.sacrifice_fly as batting_result_sacrifice_fly', 'player_batting_results.stolen_base as batting_result_stolen_base', 'player_batting_results.caught_stealing as batting_result_caught_stealing', 'player_batting_results.doubleplay as batting_result_doubleplay'];
        $pitcher_selects = ['player_pitching_results.year as pitching_result_year', 'player_pitching_results.game as pitching_result_game', 'player_pitching_results.starter as pitching_result_starter', 'player_pitching_results.plate_appearance as pitching_result_plate_appearance', 'player_pitching_results.getout as pitching_result_getout', 'player_pitching_results.allowed_runs as pitching_result_allowed_runs', 'player_pitching_results.earned_runs as pitching_result_earned_runs', 'player_pitching_results.allowed_hits as pitching_result_allowed_hits', 'player_pitching_results.allowed_homeruns as pitching_result_allowed_homeruns', 'player_pitching_results.strikeouts as pitching_result_strikeouts', 'player_pitching_results.bases_on_balls as pitching_result_bases_on_balls', 'player_pitching_results.hit_by_pitches as pitching_result_hit_by_pitches', 'player_pitching_results.wins as pitching_result_wins', 'player_pitching_results.losses as pitching_result_losses', 'player_pitching_results.complete_games as pitching_result_complete_games', 'player_pitching_results.shutouts as pitching_result_shutouts', 'player_pitching_results.holds as pitching_result_holds', 'player_pitching_results.saves as pitching_result_saves'];

        $players = DB::table('possessed_players')
                   ->select(array_merge($player_selects, $batter_selects, $pitcher_selects))
                   ->join('players', 'possessed_players.player_id', '=', 'players.id')
                   ->leftJoin('player_batting_results', function ($join) {
                       $join->on('possessed_players.player_id', '=', 'player_batting_results.player_id')->on('possessed_players.age', '=', 'player_batting_results.player_age');
                   })
                   ->leftJoin('player_pitching_results', function ($join) {
                       $join->on('possessed_players.player_id', '=', 'player_pitching_results.player_id')->on('possessed_players.age', '=', 'player_pitching_results.player_age');
                   })
                   ->where('possessed_players.team_id', $team->id)
                   ->whereIn('players.id', $team_order->order_lineup->getLineupPlayerIds())
                   ->get()->toArray();
        $players = array_map(function($player) {

            $batting_status = ($player->batting_result_year === null) ? null :[
                'teams_game' => Player::$teams_game_of_year[$player->batting_result_year],
                'game' => $player->batting_result_game,
                'plate_appearance' => $player->batting_result_plate_appearance,
                'hits' => $player->batting_result_hits,
                'twobase' => $player->batting_result_twobase,
                'triple' => $player->batting_result_triple,
                'homerun' => $player->batting_result_homerun,
                'rbi' => $player->batting_result_rbi,
                'fourball' => $player->batting_result_fourball + $player->batting_result_deadball,
                'strikeout' => $player->batting_result_strikeout,
                'sacrifice_bunt' => $player->batting_result_sacrifice_bunt,
                'sacrifice_fly' => $player->batting_result_sacrifice_fly,
                'stolen_base' => $player->batting_result_stolen_base,
                'caught_stealing' => $player->batting_result_caught_stealing,
                'doubleplay' => $player->batting_result_doubleplay,
            ];

            $pitching_status = ($player->pitching_result_year === null) ? null : [
                'teams_game' => Player::$teams_game_of_year[$player->pitching_result_year],
                'game' => $player->pitching_result_game,
                'game_starter' => $player->pitching_result_starter,
                'plate_appearance' => $player->pitching_result_plate_appearance,
                'getout' => $player->pitching_result_getout,
                'allowed_runs' => $player->pitching_result_allowed_runs,
                'earned_runs' => $player->pitching_result_earned_runs,
                'allowed_hits' => $player->pitching_result_allowed_hits,
                'allowed_homeruns' => $player->pitching_result_allowed_homeruns,
                'strikeouts' => $player->pitching_result_strikeouts,
                'bases_on_balls' => $player->pitching_result_bases_on_balls + $player->pitching_result_hit_by_pitches,
                'wins' => $player->pitching_result_wins,
                'losses' => $player->pitching_result_losses,
                'complete_games' => $player->pitching_result_complete_games,
                'shutouts' => $player->pitching_result_shutouts,
                'holds' => $player->pitching_result_holds,
                'saves' => $player->pitching_result_saves,
            ];

            return [
                'id' => $player->player_id,
                'name' => $player->player_name,
                'battingStatus' => $batting_status,
                'pitchingStatus' => $pitching_status,
                'fieldablePositions' => json_decode($player->player_positions, true),
            ];
        }, $players);

        return [
            'order' => $order,
            'positions' => $positions,
            'bench_player_ids' => $bench_player_ids,
            'players' => $players,
        ];

    }

    /*
     * 全チーム数が奇数の時余ったチームと対戦させるためのCPUチーム
     * 野手12人と投手5人のチーム
     */
    private static function makeSubstituteTeamOrderArray($is_dh_game)
    {
        $order = [];
        $positions = [];
        $bench_player_ids = [];
        $players = [];

        $acquired_player_ids = []; // 重複防止のため
        // 野手12人ガチャのため利用
        $position_arr = [Position::catcher, Position::first, Position::second, Position::third,
                        Position::shortstop, Position::left, Position::center, Position::right]; // ''は特に指定がないことを表す（LIKE検索でそのまま使うため)
        if ($is_dh_game === true) $position_arr[] = Position::designated_hitter;
        shuffle($position_arr);
        for ($i = count($position_arr); $i < 12; $i++) {
            $position_arr[] = Position::designated_hitter;
        }

        // 野手12人のガチャを実行
        for ($i = 0; $i < 12; $i++) {
            $position = $position_arr[$i];
            if ($position === Position::designated_hitter) {
                // 野手なら誰でも
                $player = Player::with('player_batting_results')->where('positions', 'not like', '%"pitcher"%')->where('rate', 10)->whereNotIn('id', $acquired_player_ids)->get()->random();
            } else {
                // 指定ポジションを守れる選手
                $position_like = '%"' . $position . '"%';
                $player = Player::with('player_batting_results')->where('positions', 'like', $position_like)->where('rate', 10)->whereNotIn('id', $acquired_player_ids)->get()->random();
            }
            $acquired_player_ids[] = $player->id;

            $starting_count = ($is_dh_game === true) ? 9 : 8;
            if ($i < $starting_count) {
                // スタメン野手追加
                $batting_order = $i + 1;
                $order_key = 'order' . $batting_order;
                $order[$order_key] = $player->id;

                $positions[$position] = $batting_order;
            } else {
                // ベンチ野手追加
                $bench_player_ids[$player->id] = Role::dependable_pinch_hitter;
            }
                
            // playersに追加
            $allowed = array_flip(['year', 'game', 'plate_appearance', 'hits', 'twobase', 'triple', 'homerun', 'rbi', 'fourball', 'deadball', 'strikeout', 'sacrifice_bunt', 'sacrifice_fly', 'stolen_base', 'caught_stealing', 'doubleplay']);
            $batting_status = array_intersect_key($player->player_batting_results->sortBy('year')->first()->toArray(), $allowed);
            $batting_status['teams_game'] = Player::$teams_game_of_year[$batting_status['year']];
            $batting_status['fourball'] += $batting_status['deadball'];
            unset($batting_status['year'], $batting_status['deadball']);

            $players[] = [
                'id' => $player->id,
                'name' => $player->name,
                'battingStatus' => $batting_status,
                'pitchingStatus' => null,
                'fieldablePositions' => json_decode($player->positions, true),
            ];
        }

        // 投手5人のガチャを実行
        for ($i = 0; $i < 5; $i++) {
            $player = Player::with('player_pitching_results')->where('positions', 'like', '%"pitcher"%')->where('rate', 10)->whereNotIn('id', $acquired_player_ids)->get()->random();
            $acquired_player_ids[] = $player->id;

            if ($i === 0) {
                // 先発投手追加
                if ($is_dh_game === true) {
                    $order['order0'] = $player->id;
                    $positions[Position::pitcher] = 0;
                } else {
                    $order['order9'] = $player->id;
                    $positions[Position::pitcher] = 9;
                }
            } else {
                // リリーフ投手追加
                switch ($i) {
                    case 1:
                        $role = Role::long_relief;break;
                    case 2:
                        $role = Role::pre_setup_pitcher;break;
                    case 3:
                        $role = Role::setup_pitcher;break;
                    case 4:
                        $role = Role::closer;break;
                }
                $bench_player_ids[$player->id] = $role;
            }

            // playersに追加
            $allowed = array_flip(['year', 'game', 'game_starter', 'plate_appearance', 'getout', 'allowed_runs', 'earned_runs', 'allowed_hits', 'allowed_homeruns', 'strikeouts', 'bases_on_balls', 'hit_by_pitches', 'wins', 'losses', 'complete_games', 'shutouts', 'holds', 'saves']);
            $pitching_status = array_intersect_key($player->player_pitching_results->sortBy('year')->first()->toArray(), $allowed);
            $pitching_status['teams_game'] = Player::$teams_game_of_year[$pitching_status['year']];
            $pitching_status['bases_on_balls'] += $pitching_status['hit_by_pitches'];
            unset($pitching_status['year'], $pitching_status['hit_by_pitches']);

            $players[] = [
                'id' => $player->id,
                'name' => $player->name,
                'battingStatus' => null,
                'pitchingStatus' => $pitching_status,
                'fieldablePositions' => json_decode($player->positions, true),
            ];
        }

        return [
            'order' => $order,
            'positions' => $positions,
            'bench_player_ids' => $bench_player_ids,
            'players' => $players,
        ];

    }

}

