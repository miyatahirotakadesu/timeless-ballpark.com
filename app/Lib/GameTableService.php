<?php

namespace App\Lib;

use App\Models\Game as Game;
use App\Models\Team as Team;

class GameTableService
{

    public static function makeGameTable($game_time)
    {
        $team_ids = Team::select('id')->get()->pluck('id')->toArray();
        shuffle($team_ids);
        $team_id_pairs = array_chunk($team_ids, 2);

        $games = [];
        foreach ($team_id_pairs as $team_id_pair) {
            $home_team_id = $team_id_pair[0];
            $visitor_team_id = (isset($team_id_pair[1])) ? $team_id_pair[1] : -1;
            $date = date('Y-m-d H:i:s');
            $games[] = [
                'game_number' => $game_time,
                'executed' => false,
                'home_team_id' => $home_team_id,
                'visitor_team_id' => $visitor_team_id,
                'home_team_score' => null,
                'visitor_team_score' => null,
                'created_at' => $date,
                'updated_at' => $date,
            ];
        }
        Game::insert($games);

    }

}

