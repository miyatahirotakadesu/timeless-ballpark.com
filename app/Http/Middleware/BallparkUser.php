<?php

namespace App\Http\Middleware;

use App\Models\User as User;
use App\Models\UserAuth as UserAuth;
use App\Models\Team as Team;

use Closure;
use Session;

class BallparkUser
{
    public function handle($request, Closure $next, $guard = null)
    {
        // $request->user付与
        $user_auth = UserAuth::with('user')->where('id', Session::get('user_auth_id'))->first();
        $request->user = (isset($user_auth->user)) ? $user_auth->user : null;
        if (isset($request->user)) $user_auth->user->name = $user_auth->name;

        // $request->team付与
        if (isset($user_auth->user, $user_auth->user->id)) {
            $request->team = Team::where('user_id', $user_auth->user->id)->first();
        } else {
            $request->team = Team::where('session_id', Session::getId())->first();
        }

        return $next($request);
    }
}
