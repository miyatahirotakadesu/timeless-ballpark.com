<?php

namespace App\Http\Middleware;

use App\Models\SystemParam as SystemParam;

use Closure;
use Session;

class CheckUpdateGameTime
{
    /*
     * ランキングAPIや試合一覧APIに対して使われる想定
     * ゲーム内時間（system_paramsのkey=game_timeの値）が進み試合が行われていなければ、
     * ランキングAPIや試合一覧APIが返すデータは同じとなる
     * 負荷削減のため、以下を実装している
     *
     * ・$request->game_timeが付与されていない→最新のデータを返す（クライアントの最初のAPIアクセス）
     * ・$request->game_timeが付与されている→SysteParam.getSystemParam('game_time')値と異なれば最新のデータを返す（クライアントの２回目以降のAPIアクセス）
     *
     * ・最新のデータを返す時にレスポンスに付与するデータ→{ modified: true, game_time: SysteParam.getSystemParam('game_time')の値 } （クライアント側は次アクセスでこれを使う想定）
     * ・最新のデータを返さない（＝何もデータを返さない）時のレスポンスデータ→{ modified: false, game_time: SysteParam.getSystemParam('game_time')の値 }
     */
    private $server_game_time;
    public function handle($request, Closure $next, $guard = null)
    {
        $this->server_game_time = SystemParam::getSystemParam(SystemParam::KEY_GAME_TIME);
        return ($request->has('game_time') && $this->server_game_time === $request->game_time) ? $this->responseWithNoData() : $this->responseWithData($request, $next);

    }

    private function responseWithData($request, Closure $next)
    {
        $response = $next($request);

        // リクエストにmodifiedとgame_timeを付与
        $content = json_decode($response->content(), true);
        $content['modified'] = true;
        $content['game_time'] = $this->server_game_time;

        $response->setContent(json_encode($content));

        return $response;
    }

    private function responseWithNoData()
    {
        // modifiedとgame_timeだけのレスポンスを返す
        return response()->json([
            'modified' => false,
            'game_time' => $this->server_game_time
        ]);
    }

}

