<?php

namespace App\Http\Middleware;

use App\Models\User as User;
use App\Models\IntroducedAccessLog as IntroducedAccessLog;

use Closure;
use DB;
use Session;

use App\Lib\GiftService as GiftService;

class CheckIntroducingCode
{

    const INTRODUCING_CODE_KEY = 'c';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // 紹介コードつきURLを踏んでアクセスしたユーザーには、誰からの紹介で来たかという情報をセッションに保持する
        if ($request->has(self::INTRODUCING_CODE_KEY)) {

            DB::beginTransaction();
            try {

                $introducing_code = $request->get(self::INTRODUCING_CODE_KEY);
                $introducing_user = User::where('introducing_code', $introducing_code)->first();
                if ($introducing_user !== null) Session::put('introduce_user_id', $introducing_user->id);

                // DBのログを見て、最近３日間のアクセスがなければ重複アクセス問題なしと判断し報酬を与える
                $recent_log_count = IntroducedAccessLog::where('user_id', $introducing_user->id)
                                                       ->where('ip_addr', $request->server('SERVER_ADDR'))
                                                       ->whereDate('created_at', '>', date('Y-m-d', strtotime('-3 day')))
                                                       ->count();
                if ($recent_log_count === 0) {
                    $team = $introducing_user->team;
                    if ($team !== null) GiftService::rewardByIntroducing($team, GiftService::INTRODUCING_TYPE_ACCESS);
                }

                // 紹介URL時アクセスログ保存
                $introduced_access_log = new IntroducedAccessLog();
                $introduced_access_log->user_id = $introducing_user->id;
                $introduced_access_log->ip_addr = $request->server('SERVER_ADDR');
                $introduced_access_log->user_agent = $request->server('HTTP_USER_AGENT');
                $introduced_access_log->referer = $request->server('HTTP_REFERER');
                $others = [];
                if ($request->server('HTTP_CLIENT_IP')) $others['HTTP_CLIENT_IP'] = $request->server('HTTP_CLIENT_IP');
                if ($request->server('HTTP_X_FORWARDED_FOR')) $others['HTTP_X_FORWARDED_FOR'] = $request->server('HTTP_X_FORWARDED_FOR');
                $introduced_access_log->others = json_encode($others, JSON_UNESCAPED_UNICODE);
                $introduced_access_log->save();

                DB::commit();
            } catch(Exception $e) {
                DB::rollback();
                throw $e;
            }

        }

        return $next($request);
    }
}
