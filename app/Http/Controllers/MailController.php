<?php

namespace App\Http\Controllers;

use App\Models\Mail as Mail;

use Illuminate\Http\Request;

use DB;
use GiftService;

class MailController extends Controller
{
    public function index(Request $request)
    {
        $mails = Mail::where('team_id', $request->team->id)->get();
        return response()->json(['mails' => $mails]);
    }

    public function read(Request $request)
    {
        $mail = Mail::where('team_id', $request->team->id)->where('id', $request->mail_id)->first();
        if ($mail === null) abort(400); // 存在しないメールID、またはユーザーに紐づいてないメールIDの場合エラー

        $mail->read_flag = true;
        $mail->save();

        return response()->json(['successful' => true]);
    }

    public function receive_gift(Request $request)
    {
        $mail = Mail::where('team_id', $request->team->id)->where('id', $request->mail_id)->first();
        if ($mail === null) abort(400); // 存在しないメールID、またはユーザーに紐づいてないメールIDの場合エラー

        DB::beginTransaction();
        try {
            GiftService::receive($request->team, $mail);
            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        } 

        return response()->json(['successful' => true]);
    }

    public function bulk(Request $request)
    {
        $mails = Mail::where('team_id', $request->team->id)->get();

        DB::beginTransaction();
        try {
            Mail::where('team_id', $request->team->id)->update(['read_flag' => true]); // 一括既読
            foreach ($mails as $mail) {
                if ($mail->received === 1) continue; // 受け取り済みメールはスルー
                GiftService::receive($request->team, $mail);
            }
            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        } 

        return response()->json(['successful' => true]);
    }

}
