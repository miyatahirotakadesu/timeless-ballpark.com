<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Enum\Position as Position;
use App\Models\Player as Player;
use App\Lib\PossessedPlayerManagerService as PossessedPlayerManagerService;

use DB;
use PlayerGacha;

class PlayerController extends Controller
{

    public function active_player(Request $request)
    {
        $batting_result_columns = ['avg', 'homerun', 'at_bats', 'rbi', 'hits', 'stolen_base', 'strikeout', 'obp', 'sacrifice_bunt', 'ops'];
        $pitching_result_columns = ['era', 'wins', 'losses', 'holds', 'saves', 'getout', 'allowed_homeruns', 'strikeouts', 'bases_on_balls', 'whip'];
        return $this->get_possess_player($request, $batting_result_columns, $pitching_result_columns);
    }

    public function possessed_player(Request $request)
    {
        $batting_result_columns = ['avg'];
        $pitching_result_columns = ['era'];
        return $this->get_possess_player($request, $batting_result_columns, $pitching_result_columns);
    }

    /*
     * active_player()とpossessed_player()の共通部分
     */
    private function get_possess_player(Request $request, $batting_result_columns, $pitching_result_columns)
    {
        $player_columns = ['players.id as player_id', 'players.name as player_name', 'players.rate as player_rate', 'players.positions as player_positions', 'possessed_players.age as player_age', 'possessed_players.retired as player_retired'];
        $batter_columns = array_map(function($column_name) {
            return 'player_batting_results.' . $column_name . ' as batting_result_' . $column_name;
        }, $batting_result_columns);
        $pitcher_columns = array_map(function($column_name) {
            return 'player_pitching_results.' . $column_name . ' as pitching_result_' . $column_name;
        }, $pitching_result_columns);

        $players = DB::table('possessed_players')
                   ->select(array_merge($player_columns, $batter_columns, $pitcher_columns) )
                   ->join('players', 'possessed_players.player_id', '=', 'players.id')
                   ->leftJoin('player_batting_results', function ($join) {
                       $join->on('possessed_players.player_id', '=', 'player_batting_results.player_id')->on('possessed_players.age', '=', 'player_batting_results.player_age');
                   })
                   ->leftJoin('player_pitching_results', function ($join) {
                       $join->on('possessed_players.player_id', '=', 'player_pitching_results.player_id')->on('possessed_players.age', '=', 'player_pitching_results.player_age');
                   })
                   ->where('possessed_players.team_id', $request->team->id)
                   ->where('possessed_players.retired', 0)
                   ->get()->toArray();
        $players = array_map(function($player) use ($batting_result_columns, $pitching_result_columns) {
            $player_batting_result = [];
            foreach ($batting_result_columns as $column_name) {
                $prop = 'batting_result_' . $column_name;
                $player_batting_result[$column_name] = $player->$prop;
            }
            $player_pitching_result = [];
            foreach ($pitching_result_columns as $column_name) {
                $prop = 'pitching_result_' . $column_name;
                $player_pitching_result[$column_name] = $player->$prop;
            }
            $has_batting_result = $player->batting_result_avg !== null;
            $has_pitching_result = $player->pitching_result_era !== null;

            $positions = json_decode($player->player_positions, true);
            $is_fielder = count(array_diff($positions, [Position::pitcher])) > 0;
            $is_pitcher = in_array(Position::pitcher, $positions);

            return [
                'id' => $player->player_id,
                'name' => $player->player_name,
                'age' => $player->player_age,
                'retired' => $player->player_retired,
                'rate' => $player->player_rate,
                'positions' => $positions,
                'is_fielder' => $is_fielder,
                'is_pitcher' => $is_pitcher,
                'player_batting_result' => $player_batting_result,
                'player_pitching_result' => $player_pitching_result,
                'has_batting_result' => $has_batting_result,
                'has_pitching_result' => $has_pitching_result,
            ];
        }, $players);

        return response()->json($players);
    }

    public function show_possessed_player(Request $request, $id)
    {
        $possessed_player = $request->team->players->where('id', $id)->first();
        if ($possessed_player === null) abort(400);

        if ($possessed_player->pivot->skip_level > 1) {
            $player = Player::with(['player_batting_results', 'player_batting_career_result', 'player_pitching_results', 'player_pitching_career_result'])
                            ->where('id', $id)->firstOrFail();
            $player->player_batting_result = $player->player_batting_results->where('player_age', $possessed_player->pivot->age)->first();
            $player->player_pitching_result = $player->player_pitching_results->where('player_age', $possessed_player->pivot->age)->first();
        } else {
            $player = Player::with([
                                'player_batting_results' => function ($q) use ($possessed_player) { $q->where('player_age', $possessed_player->pivot->age); },
                                'player_pitching_results' => function ($q) use ($possessed_player) { $q->where('player_age', $possessed_player->pivot->age); }
                              ])->where('id', $id)->firstOrFail();
            $player->player_batting_result = $player->player_batting_results->first();
            $player->player_pitching_result = $player->player_pitching_results->first();
            unset($player->player_batting_results, $player->player_pitching_results);
        }
        $player->positions = json_decode($player->positions, true);
        $player->possessed_player = $possessed_player->pivot;
        $player->has_batting_result = $player->player_batting_result !== null;
        $player->has_pitching_result = $player->player_pitching_result !== null;
        $player->is_fielder = count(array_diff($player->positions, [Position::pitcher])) > 0;
        $player->is_pitcher = in_array(Position::pitcher, $player->positions);

        return response()->json($player);
    }

    public function skip(Request $request)
    {
        $possessed_player = $request->team->players->where('id', $request->player_id)->first();
        if ($possessed_player === null) abort(400);

        // SKIP可能な保有選手であるかどうかチェック
        $skippable = $possessed_player->pivot->skip_level === 3 || $possessed_player->pivot->skip_level === 2 && $possessed_player->pivot->once_skip_executed === 0;
        if ($skippable === false) abort(400);

        // 有効な年齢であるかどうかチェック
        $player = Player::with([
                      'player_batting_results' => function ($q) use ($request) { $q->where('player_age', $request->age); },
                      'player_pitching_results' => function ($q) use ($request) { $q->where('player_age', $request->age); }
                  ])->where('id', $request->player_id)->firstOrFail();
        $exists_result = $player->player_batting_results->count() > 0 || $player->player_pitching_results->count() > 0;
        if ($exists_result === false) abort(400);

        // 年齢変更実施
        $possessed_player->pivot->age = $request->age;
        $possessed_player->pivot->once_skip_executed = true;
        $possessed_player->pivot->retired = false; // SKIP=必ず現役復帰なので
        $possessed_player->pivot->save();

        return response()->json(['successful' => true]);
    }

    const SCOUT_TYPE_NORMAL = 'normal';
    const SCOUT_TYPE_TENTIMES = 'tentimes';
    const REQUIRED_COIN_NORMAL = 100;
    const REQUIRED_COIN_TENTIMES = 1000;
    private static $scout_types = [self::SCOUT_TYPE_NORMAL, self::SCOUT_TYPE_TENTIMES];
    
    public function scout(Request $request)
    {
        if (!in_array($request->type, self::$scout_types)) abort(400);

        switch ($request->type) {
            case self::SCOUT_TYPE_NORMAL:
                $times = 1;
                $required_coin = self::REQUIRED_COIN_NORMAL;
                break;
            case self::SCOUT_TYPE_TENTIMES:
                $times = 10;
                $required_coin = self::REQUIRED_COIN_TENTIMES;
                break;
            default :
                abort(400);
        }

        DB::beginTransaction();
        try {
            if ($request->team->coin < $required_coin) throw new \Exception('coinが足りません');
            $request->team->coin -= $required_coin;
            $request->team->save();
            $gacha_result = PlayerGacha::execGacha($request->team, $times);

            // ベンチが空いていれば埋める
            $possessed_player_manager_service = new PossessedPlayerManagerService($request->team);
            $possessed_player_manager_service->formatTeamBench();

            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        } 

        return response()->json($gacha_result);

    }

}
