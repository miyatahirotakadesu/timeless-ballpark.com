<?php

namespace App\Http\Controllers;

use App\Models\Game as Game;
use App\Models\Team as Team;

use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index(Request $request)
    {
        $response_arr = [];
        if (isset($request->team)) {
            // 自分のチームの全試合結果
            $response_arr['games'] = Game::with([
                                        'home_team' => function ($q) { return $q->select(['id', 'name']); },
                                        'visitor_team' => function ($q) { return $q->select(['id', 'name']); }
                                     ])
                                     ->where(function($query) use ($request) {
                                         $query->where('home_team_id', $request->team->id)
                                         ->orWhere('visitor_team_id', $request->team->id);
                                     })
                                     ->orderBy('game_number', 'desc')
                                     ->select(['id', 'game_number', 'executed', 'home_team_id', 'visitor_team_id', 'home_team_score', 'visitor_team_score', 'walkoff_game_flg', 'created_at'])
                                     ->get();
        } else {
            // チームがないクライアントには全チームの試合の最新１００件を返す
            $response_arr['games'] = Game::with([
                                        'home_team' => function ($q) { return $q->select(['id', 'name']); },
                                        'visitor_team' => function ($q) { return $q->select(['id', 'name']); }
                                     ])
                                     ->where('executed', true)
                                     ->orderBy('game_number', 'desc')
                                     ->select(['id', 'game_number', 'executed', 'home_team_id', 'visitor_team_id', 'home_team_score', 'visitor_team_score', 'walkoff_game_flg', 'created_at'])
                                     ->take(100)
                                     ->get();
        }
        return response()->json($response_arr);
    }

    public function show(Request $request, $id)
    {
        $game = Game::with([
                        'home_team' => function ($q) { return $q->select(['id', 'name']); },
                        'visitor_team' => function ($q) { return $q->select(['id', 'name']); }
                    ])
                    ->where('id', $id)->firstOrFail();
        return response()->json(['game' => $game]);
    }

}
