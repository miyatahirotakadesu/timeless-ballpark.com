<?php

namespace App\Http\Controllers;

use App\Models\SystemParam as SystemParam;

use Illuminate\Http\Request;

class SystemParamController extends Controller
{
    public function index(Request $request)
    {
        $response_arr = [
            'season' => SystemParam::getSystemParam(SystemParam::KEY_SEASON),
            'game_time' => SystemParam::getSystemParam(SystemParam::KEY_GAME_TIME),
        ];
        return response()->json($response_arr);
    }
}

