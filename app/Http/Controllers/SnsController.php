<?php

namespace App\Http\Controllers;

use App\Models\User as User;
use App\Models\UserAuth as UserAuth;
use App\Models\Team as Team;

use Illuminate\Http\Request;

use App\Lib\GiftService as GiftService;

use DB;
use Session;
use Socialite;

class SnsController extends Controller
{

    const SNS_PURPOSE_LOGIN = 10;
    const SNS_PURPOSE_ATTACH = 20;
    const SNS_PURPOSE_DETACH = 30;

    private static $sns_purposes = [
        self::SNS_PURPOSE_LOGIN, self::SNS_PURPOSE_ATTACH, self::SNS_PURPOSE_DETACH
    ];

    public function login(Request $request, $driver)
    {
        if (!in_array($driver, UserAuth::getDrivers())) abort(404);
        Session::put('sns_purpose', self::SNS_PURPOSE_LOGIN);
        // 「SNSログイン目的」というセッション保持したあとリダイレクト
        return Socialite::driver($driver)->redirect();
    }

    public function attach(Request $request, $driver)
    {
        if (!in_array($driver, UserAuth::getDrivers())) abort(404);
        Session::put('sns_purpose', self::SNS_PURPOSE_ATTACH);
        // 「SNS連携目的」というセッション保持したあとリダイレクト
        return Socialite::driver($driver)->redirect();
    }

    public function detach(Request $request, $driver)
    {
        if (!in_array($driver, UserAuth::getDrivers())) abort(404);
        // SNS連携解除目的
        $auth_type = array_search($driver, UserAuth::getDrivers());
        $user_auth = $request->user->user_auths->where('auth_type', $auth_type)->first();

        DB::beginTransaction();
        try {
            if ($user_auth === null) return redirect('/sns'); // すでに連携解除されている場合
            if ($request->user->user_auths->count() < 2) return redirect('/sns'); // 連携解除することでログインできなくなってしまう場合

            if ($user_auth->id === Session::get('user_auth_id')) {
                // 連携解除によって今ログイン中のuser_authが失われてしまう場合は、セッションを書き換えて逃げておく
                $old_user_auth_id = Session::get('user_auth_id');
                $new_user_auth_id = $request->user->user_auths->pluck('id')->filter(function($id) use ($old_user_auth_id) {
                    return $id !== $old_user_auth_id;
                })->first();
                Session::put('user_auth_id', $new_user_auth_id);
            }
            $user_auth->delete();

            DB::commit();
            return redirect('/sns');
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        } 
        return redirect('/sns');
    }

    public function callback(Request $request, $driver)
    {
        if (!in_array($driver, UserAuth::getDrivers())) abort(404);
        // 何のためのSNS認証であったかをセッションから読み取り
        $sns_purpose = Session::pull('sns_purpose');
        if (!in_array($sns_purpose, self::$sns_purposes)) abort(404);

        $socialite_user = Socialite::driver($driver)->user();
        $auth_type = array_search($driver, UserAuth::getDrivers());
        $user_auth = UserAuth::where('auth_type', $auth_type)->where('auth_id', $socialite_user->getId())->first();

        DB::beginTransaction();
        try {
            if ($sns_purpose === self::SNS_PURPOSE_LOGIN) {
                // SNSログイン目的
                if ($user_auth === null) {
                    // 会員登録
                    $user = new User();
                    if (Session::has('introduce_user_id')) {
                        // 誰かの紹介により新規登録した場合
                        $user->user_id_introduced_by = Session::get('introduce_user_id');
                        $introducing_user = User::where('id', $user->user_id_introduced_by)->firstOrFail();
                        if ($introducing_user->team !== null) GiftService::rewardByIntroducing($introducing_user->team, GiftService::INTRODUCING_TYPE_REGIST);
                    }
                    $user->initIntroducingCode();
                    $user->save();

                    $user_auth = new UserAuth();
                    $user_auth->user_id = $user->id;
                    $user_auth->auth_type = $auth_type;
                    $user_auth->auth_id = $socialite_user->getId();
                    $user_auth->name = $socialite_user->getName();
                    if (in_array($auth_type, [UserAuth::AUTH_TYPE_TWITTER, UserAuth::AUTH_TYPE_FACEBOOK])) {
                        $user_auth->token = $socialite_user->token;
                        $user_auth->token_secret = property_exists($socialite_user, 'tokenSecret') ? $socialite_user->tokenSecret : null;
                    }
                    $user_auth->save();

                    // ユーザー登録前にチームの作成は行っている場合、そのチームと紐付けを行う
                    if (isset($request->team)) {
                        $request->team->user_id = $user->id;
                        $request->team->name = $user_auth->name;
                        $request->team->save();
                    }
                }
                Session::put('user_auth_id', $user_auth->id); // ログイン
                DB::commit();
                return redirect('/');
            } else if ($sns_purpose === self::SNS_PURPOSE_ATTACH) {
                // SNS連携目的
                if ($user_auth !== null) return redirect('/sns'); // すでに連携している場合

                $user_auth = new UserAuth();
                $user_auth->user_id = $request->user->id;
                $user_auth->auth_type = $auth_type;
                $user_auth->auth_id = $socialite_user->getId();
                $user_auth->name = $socialite_user->getName();
                if (in_array($auth_type, [UserAuth::AUTH_TYPE_TWITTER, UserAuth::AUTH_TYPE_FACEBOOK])) {
                    $user_auth->token = $socialite_user->token;
                    $user_auth->token_secret = property_exists($socialite_user, 'tokenSecret') ? $socialite_user->tokenSecret : null;
                }
                $user_auth->save();

                DB::commit();
                return redirect('/sns');
            }
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        } 

        return redirect('/');
    }
}

