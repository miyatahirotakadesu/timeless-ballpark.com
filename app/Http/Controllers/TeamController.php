<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Enum\Position as Position;

use App\Models\SystemParam as SystemParam;
use App\Models\Game as Game;
use App\Models\Team as Team;
use App\Models\TeamOrder as TeamOrder;
use App\Models\Columns\OrderLineup as OrderLineup;

use DB;
use Session;
use PlayerGacha;
use TeamOrderService;

class TeamController extends Controller
{

    const CHANGING_TEAM_NAME_COIN = 500;

    public function auth(Request $request)
    {
        $response = ['team' => $request->team];
        if (isset($request->team) && $request->has('login')) { 
            $response['is_accrued_login_bonus'] = $request->team->execLogin(); // ログインボーナス処理
        }
        return response()->json($response);
    }

    public function show(Request $request, $id)
    {
        // チーム情報を取得
        $team = Team::with(['team_orders', 'team_records'])->where('id', $id)->firstOrFail();

        // オーダー用選手情報取得
        $player_ids = []; // 控えや二軍の全選手を取得する必要はないため絞り込みのためのid配列
        $team->team_orders->each(function($team_order) use (&$player_ids) {
            $add_player_ids = $team_order->order_lineup->getLineupPlayerIds(['bench_player_roles' => false]);
            $player_ids = array_merge($player_ids, $add_player_ids);
        });

        $player_columns = ['players.id as player_id', 'players.name as player_name', 'players.rate as player_rate',  'possessed_players.age as player_age'];
        $batting_result_columns = ['avg', 'homerun', 'rbi'];
        $pitching_result_columns = ['era', 'wins', 'losses', 'holds', 'saves'];
        $batter_columns = array_map(function($column_name) {
            return 'player_batting_results.' . $column_name . ' as batting_result_' . $column_name;
        }, $batting_result_columns);
        $pitcher_columns = array_map(function($column_name) {
            return 'player_pitching_results.' . $column_name . ' as pitching_result_' . $column_name;
        }, $pitching_result_columns);

        $players = DB::table('possessed_players')
                   ->select(array_merge($player_columns, $batter_columns, $pitcher_columns) )
                   ->join('players', 'possessed_players.player_id', '=', 'players.id')
                   ->leftJoin('player_batting_results', function ($join) {
                       $join->on('possessed_players.player_id', '=', 'player_batting_results.player_id')->on('possessed_players.age', '=', 'player_batting_results.player_age');
                   })
                   ->leftJoin('player_pitching_results', function ($join) {
                       $join->on('possessed_players.player_id', '=', 'player_pitching_results.player_id')->on('possessed_players.age', '=', 'player_pitching_results.player_age');
                   })
                   ->where('possessed_players.team_id', $id)
                   ->whereIn('possessed_players.player_id', $player_ids)
                   ->get()->toArray();

        $players = array_map(function($player) use ($batting_result_columns, $pitching_result_columns) {
            $player_batting_result = [];
            foreach ($batting_result_columns as $column_name) {
                $prop = 'batting_result_' . $column_name;
                $player_batting_result[$column_name] = $player->$prop;
            }
            $player_pitching_result = [];
            foreach ($pitching_result_columns as $column_name) {
                $prop = 'pitching_result_' . $column_name;
                $player_pitching_result[$column_name] = $player->$prop;
            }
            $has_batting_result = $player->batting_result_avg !== null;
            $has_pitching_result = $player->pitching_result_era !== null;

            return [
                'id' => $player->player_id,
                'name' => $player->player_name,
                'age' => $player->player_age,
                'rate' => $player->player_rate,
                'player_batting_result' => $player_batting_result,
                'player_pitching_result' => $player_pitching_result,
                'has_batting_result' => $has_batting_result,
                'has_pitching_result' => $has_pitching_result,
            ];
        }, $players);

        return response()->json(['team' => $team, 'players' => $players]);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $team = new Team();
            if (isset($request->user)) {
                // ユーザーがログインしている時
                $team->user_id = $request->user->id;
            } else {
                // ユーザーがログインしていない時
                $team->session_id = Session::getId();
            }
            $team->coin = Team::INITIAL_COIN;
            $team->name = (isset($request->user->name)) ? $request->user->name : Team::NOUSER_TEAMNAME_PREFIX;
            $team->level = 1;
            $team->exp = 59; // Lv.N に必要な経験値は pow(30 * N, 1.2) なので
            $team->save();
            if ($team->name === Team::NOUSER_TEAMNAME_PREFIX) {
                $team->name = Team::NOUSER_TEAMNAME_PREFIX . $team->id;
                $team->save();
            }

            // 初期メンバー決定ガチャ実行
            PlayerGacha::makeTeamGacha($team);

            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        } 

        return response()->json($team);
    }

    public function show_order(Request $request)
    {
        $starter_restriction_map = TeamOrderService::makeStarterRestrictionMap($request->team->id);
        return response()->json(['team_orders' => $request->team->team_orders, 'starter_restriction_map' => $starter_restriction_map]);
    }

    public function update_order(Request $request)
    {
        $team_order = TeamOrder::where('team_id', $request->team->id)->where('order_type', $request->order_type)->firstOrFail();

        $order_lineup = new OrderLineup(json_encode($request->content));
        TeamOrderService::validateOrRepair($request->team, $request->order_type, $order_lineup);

        $team_order->content = json_encode($request->content);
        $team_order->save();
        return response()->json(['successful' => true]);
    }

    public function update_name(Request $request)
    {
        if ($request->team->coin < self::CHANGING_TEAM_NAME_COIN || $request->team->changed_name !== 0) abort(400);

        // 入力値チェック
        $this->validate($request, [
            'teamname' => 'required|max:16',
        ], [
            'teamname.required' => 'チーム名を入力してください',
            'teamname.max' => 'チーム名は16文字以内で入力してください',
        ]);

        DB::beginTransaction();
        try {
            $request->team->name = $request->teamname;
            $request->team->coin -= 500;
            $request->team->changed_name = true;
            $request->team->save();

            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        }
        return response()->json(['successful' => true]);
    }
        

}
