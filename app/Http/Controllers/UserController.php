<?php

namespace App\Http\Controllers;

use App\Models\User as User;
use App\Models\UserAuth as UserAuth;

use Illuminate\Http\Request;

use DB;
use Session;

class UserController extends Controller
{

    public function auth(Request $request)
    {
        $response = ['user' => $request->user];
        return response()->json($response);
    }

    public function signup(Request $request)
    {
        // 重複チェック
        $overlapped_user_auth = UserAuth::where('auth_type', UserAuth::AUTH_TYPE_IDPASS)->where('auth_id', $request->id)->first();
        if ($overlapped_user_auth !== null) return response()->json(['id' => '既に登録されているIDです'], 422);

        // 入力値チェック
        $this->validate($request, [
            'id' => 'required|max:64',
            'password' => 'required|max:64',
        ], [
            'id.required' => 'IDを入力してください',
            'id.max' => 'IDは64文字以内で入力してください',
            'password.required' => 'パスワードを入力してください',
            'password.max' => 'パスワードは64文字以内で入力してください',
        ]);
 
        // 登録処理
        DB::beginTransaction();
        try {
            $user = new User();
            // 誰かの紹介により新規登録した場合であっても、不正防止のために、ID/PASS登録の場合は紹介登録とみなさない
            $user->initIntroducingCode();
            $user->save();

            $user_auth = new UserAuth();
            $user_auth->user_id = $user->id;
            $user_auth->auth_type = UserAuth::AUTH_TYPE_IDPASS;
            $user_auth->auth_id = $request->id;
            $user_auth->password = hash_hmac('sha256', $request->password, 'timelessballpark256');
            $user_auth->name = $request->id;
            $user_auth->save();

            // ユーザー登録前にチームの作成は行っている場合、そのチームと紐付けを行う
            if (isset($request->team)) {
                $request->team->user_id = $user->id;
                $request->team->name = $user_auth->name;
                $request->team->save();
            }
            Session::put('user_auth_id', $user_auth->id); // ログイン

            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
            throw $e;
        } 

        return response()->json(['successful' => true]);

    }

    public function auth_type(Request $request)
    {
        $auth_types = $request->user->user_auths->pluck('auth_type')->map(function($auth_type) {
            return UserAuth::getAuthTypeDriver($auth_type);
        });
        return response()->json(['auth_types' => $auth_types]);
    }

    public function login(Request $request)
    {
        $user_auth = UserAuth::where('auth_id', $request->id)->where('password', hash_hmac('sha256', $request->password, 'timelessballpark256'))->first();
        if ($user_auth === null) return response()->json(['id' => 'ログインに失敗しました'], 422);

        Session::put('user_auth_id', $user_auth->id); // ログイン
        return response()->json(['successful' => true]);
    }

    public function logout(Request $request)
    {
        Session::flush();
        Session::regenerate();
        return response()->json(['successful' => true]);
    }
}
