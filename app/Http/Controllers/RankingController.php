<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use RankingService;

class RankingController extends Controller
{
    public function index(Request $request)
    {
        return response()->json(['ranking' => RankingService::getRanking()]);
    }
}
