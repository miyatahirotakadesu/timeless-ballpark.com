<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RankingServiceProvicer extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(
            'ranking_service',
            'App\Lib\RankingService'
        );
    }
}
