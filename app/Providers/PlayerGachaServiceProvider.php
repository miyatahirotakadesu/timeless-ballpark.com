<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PlayerGachaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'player_gacha',
            'App\Lib\PlayerGacha'
        );
    }
}
