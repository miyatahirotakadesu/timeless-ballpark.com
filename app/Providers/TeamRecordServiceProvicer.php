<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TeamRecordServiceProvicer extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'team_record_service',
            'App\Lib\TeamRecordService'
        );
    }
}
