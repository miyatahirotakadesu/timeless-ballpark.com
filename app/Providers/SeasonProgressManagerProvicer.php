<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SeasonProgressManagerProvicer extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'season_progress_manager',
            'App\Lib\SeasonProgressManager'
        );
    }
}
