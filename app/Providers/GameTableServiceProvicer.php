<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GameTableServiceProvicer extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'game_table_service',
            'App\Lib\GameTableService'
        );
    }
}
