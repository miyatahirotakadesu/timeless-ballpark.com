<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TeamOrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'team_order_service',
            'App\Lib\TeamOrderService'
        );
    }
}
