<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PossessedPlayerManagerServiceProvicer extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'possessed_player_manager_service',
            'App\Lib\PossessedPlayerManagerService'
        );
    }
}
