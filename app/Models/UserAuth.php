<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAuth extends Model
{
    const AUTH_TYPE_IDPASS = 10;
    const AUTH_TYPE_TWITTER = 20;
    const AUTH_TYPE_FACEBOOK = 30;
    const AUTH_TYPE_LINE = 40;
    const AUTH_TYPE_GOOGLE = 50;
    const AUTH_TYPE_YAHOO = 60;

    private static $drivers = [
        UserAuth::AUTH_TYPE_IDPASS => 'idpass',
        UserAuth::AUTH_TYPE_TWITTER => 'twitter',
        UserAuth::AUTH_TYPE_FACEBOOK => 'facebook',
        UserAuth::AUTH_TYPE_LINE => 'line',
        UserAuth::AUTH_TYPE_GOOGLE => 'google',
        UserAuth::AUTH_TYPE_YAHOO => 'yahoo',
    ];

    public static function getDrivers()
    {
        return self::$drivers;
    }

    public static function getAuthTypeDriver($auth_type)
    {
        if (!array_key_exists($auth_type, self::$drivers)) throw new \LogicException('auth_typeに対応するSocialDriverが存在しません');
        return self::$drivers[$auth_type];
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
