<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    //
    public function player_batting_results()
    {
        return $this->hasMany('App\Models\PlayerBattingResult');
    }

    public function player_batting_career_result()
    {
        return $this->hasOne('App\Models\PlayerBattingCareerResult');
    }

    public function player_pitching_results()
    {
        return $this->hasMany('App\Models\PlayerPitchingResult');
    }

    public function player_pitching_career_result()
    {
        return $this->hasOne('App\Models\PlayerPitchingCareerResult');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Models\Team', 'possessed_players');
    }

    public static $teams_game_of_year = [
        1936 => 32,
        1937 => 50,
        1938 => 50,
        1939 => 96,
        1940 => 104,
        1941 => 84,
        1942 => 105,
        1943 => 84,
        1944 => 105,
        1946 => 119,
        1947 => 140,
        1948 => 104,
        1949 => 135,
        1950 => 140,
        1951 => 120,
        1952 => 120,
        1953 => 130,
        1954 => 130,
        1955 => 130,
        1956 => 130,
        1957 => 130,
        1958 => 130,
        1959 => 130,
        1960 => 130,
        1961 => 130,
        1962 => 130,
        1963 => 140,
        1964 => 140,
        1965 => 140,
        1966 => 130,
        1967 => 130,
        1968 => 130,
        1969 => 130,
        1970 => 130,
        1971 => 130,
        1972 => 130,
        1973 => 130,
        1974 => 130,
        1975 => 130,
        1976 => 130,
        1977 => 130,
        1978 => 130,
        1979 => 130,
        1980 => 130,
        1981 => 130,
        1982 => 130,
        1983 => 130,
        1984 => 130,
        1985 => 130,
        1986 => 130,
        1987 => 130,
        1988 => 130,
        1989 => 130,
        1990 => 130,
        1991 => 130,
        1992 => 130,
        1993 => 130,
        1994 => 130,
        1995 => 130,
        1996 => 130,
        1997 => 135,
        1998 => 135,
        1999 => 135,
        2000 => 135,
        2001 => 140,
        2002 => 140,
        2003 => 140,
        2004 => 140,
        2005 => 146,
        2006 => 146,
        2007 => 144,
        2008 => 144,
        2009 => 144,
        2010 => 144,
        2011 => 144,
        2012 => 144,
        2013 => 144,
        2014 => 144,
        2015 => 143,
        2016 => 143,
        2017 => 143,
    ];

}
