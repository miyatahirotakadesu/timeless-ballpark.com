<?php

namespace App\Models;

use App\Models\Columns\OrderLineup as OrderLineup;

use Illuminate\Database\Eloquent\Model;

class TeamOrder extends Model
{

    protected $appends = ['order_lineup'];

    /* 未使用 const ORDER_TYPE_WITHOUT_DH = 10; */ // DHなしオーダー
    const ORDER_TYPE_WITH_DH = 20; // DHありオーダー

    const BENCH_BATTER_COUNT = 7; // 控え野手数
    const BENCH_PITCHER_COUNT = 7; // 控え投手数

    private $order_lineup; // OrderLineup型

    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }

    public function getOrderLineupAttribute()
    {
        if ($this->order_lineup === null) $this->order_lineup = new OrderLineup($this->content);
        return $this->order_lineup;
    }


}
