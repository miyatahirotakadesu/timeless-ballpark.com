<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public function team()
    {
        return $this->hasOne('App\Models\Team');
    }

    public function user_auths()
    {
        return $this->hasMany('App\Models\UserAuth');
    }

    /*
     * １つでも紐づいているSNSログインアカウントを持っているか
     * 紹介報酬判定に利用
     */
    public function hasSnsAuth()
    {
        return $this->whereHas('user_auths', function($q) {
            $q->whereNotIn('auth_type', [UserAuth::AUTH_TYPE_IDPASS]);
        })->count > 0;
    }

    /*
     * 招待コードを生成する
     * ダブらないようにループしている
     */
    public function initIntroducingCode()
    {
        do {
            $introducing_code = mb_strtolower(str_random(6));
            $duplicate_user_count = User::where('introducing_code', $introducing_code)->count();
        } while ($duplicate_user_count > 0);
        $this->introducing_code = $introducing_code;
    }

}
