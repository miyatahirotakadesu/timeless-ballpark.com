<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemParam extends Model
{
    const KEY_SEASON = 'season';
    const KEY_GAME_TIME = 'game_time';

    private static $keys = [self::KEY_SEASON, self::KEY_GAME_TIME];
    private static $default_values = [
        self::KEY_SEASON => '0',
        self::KEY_GAME_TIME => '0',
    ];

    /*
     * テーブルのvalue値を返す
     */
    public static function getSystemParam($key)
    {
        if (!in_array($key, self::$keys)) throw new \LogicException($key . ' は存在しないSystemParamのキー名です');

        $system_param = self::where('key', $key)->first();
        if ($system_param === null) $system_param = self::seed($key);

        return $system_param->value;
    }

    /*
     * テーブルのvalue値を更新する
     */
    public static function setSystemParam($key, $value)
    {
        if (!in_array($key, self::$keys)) throw new \LogicException($key . ' は存在しないSystemParamのキー名です');

        $system_param = self::where('key', $key)->first();
        if ($system_param === null) $system_param = self::seed($key);

        $system_param->value = $value;
        return $system_param->save();
    }

    /*
     * キーバリューペアのレコードを生成する。各ペアのレコードがない最初の１回目のみ利用される。
     */
    private static function seed($key)
    {
        if (!in_array($key, self::$keys)) throw new \LogicException($key . ' は存在しないSystemParamのキー名です');

        $system_param = new static();
        $system_param->key = $key;
        $system_param->value = self::$default_values[$key];
        $system_param->save();

        return $system_param;
    }


}
