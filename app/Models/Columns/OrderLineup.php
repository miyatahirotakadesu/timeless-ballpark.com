<?php

namespace App\Models\Columns;

use App\Enum\Role as Role;
use App\Enum\Position as Position;

use App\Models\TeamOrder as TeamOrder;

class OrderLineup
{
    /*
     * team_orders.contentカラムのJSONデータに対応したデータ型
     */

    public $starting_order;
    public $starter_pitchers;
    public $bench_player_roles;

    private $changed_without_dh = false; // toWithoutDHOrder()を通ってDHなし向けのオーダーに変更されたかどうかの状態を表す。

    public function __construct($jsontext)
    {
        $order_lineup_arr = json_decode($jsontext, true);
        if ($order_lineup_arr === null || !array_key_exists('starting_order', $order_lineup_arr) || !array_key_exists('starter_pitchers', $order_lineup_arr) || !array_key_exists('bench_player_roles', $order_lineup_arr)) {
            throw new \RuntimeException('オーダーの形式が不正です');
        }

        $map_to_obj = function ($arr) { return (object)$arr; };

        $starting_order_objs = array_map($map_to_obj, $order_lineup_arr['starting_order']);
        $this->starting_order = collect($starting_order_objs);
        $starter_pitchers_objs = array_map($map_to_obj, $order_lineup_arr['starter_pitchers']);
        $this->starter_pitchers = collect($starter_pitchers_objs);
        $bench_player_roles_objs = array_map($map_to_obj, $order_lineup_arr['bench_player_roles']);
        $this->bench_player_roles = collect($bench_player_roles_objs);
    }

    /*
     * スタメン、先発ローテ、控えの全選手のplayer_idを返す
     * 省略したいものは$optionsに ['starting_order' => false] のように渡す
     */
    public function getLineupPlayerIds($options = [])
    {
        $starting_order_ids = [];
        $starter_pitcher_ids = [];
        $bench_player_ids = [];

        $starting_order_ids = $this->starting_order->pluck('player_id')->all();
        $starter_pitcher_ids = $this->starter_pitchers->pluck('player_id')->all();

        if (!array_key_exists('bench_player_roles', $options) || $options['bench_player_roles'] !== false) {
            $bench_player_ids = $this->bench_player_roles->pluck('player_id')->all();
        }

        return array_merge($starting_order_ids, $starter_pitcher_ids, $bench_player_ids);

    }

    /*
     * DHなしの試合の場合、指名打者をベンチに置いて打順を１つずつずらして9番に投手を入れる
     * 
     */
    public function toWithoutDHOrder()
    {
        if ($this->changed_without_dh === true) return false;

        // DH以降の打順の人の打順を１つずつ繰り上げる
        $dh_key;
        $moveup_flag = false; // スタメン走査中DHが現れたらtrueにして以後の打順を１つ上に詰めていく
        foreach ($this->starting_order as $key => $item) {
            if ($moveup_flag === true) {
                $item->batting_order--;
            }

            if ($item->position === Position::designated_hitter) {
                $dh_key = $key;
                $moveup_flag = true;
            }
        }

        // 指名打者の人はベンチに落とす
        $this->bench_player_roles->push((object)[
            'role' => Role::dependable_pinch_hitter,
            'player_id' => $this->starting_order->get($dh_key)->player_id,
        ]);
        $this->starting_order->forget($dh_key); 

        // 9番に投手を入れる
        $this->starting_order->push((object)[
            'batting_order' => 9,
            'position' => Position::pitcher
        ]); 

        $this->changed_without_dh = true;
    }

    /*
     * @return bool
     * 問題ないオーダーかどうか返す
     *
     * starting_orderが
     * -打順1〜9を埋めていること
     * -捕手〜DHの全9ポジションを埋めていること
     * -全てがplayer_idを持っていること
     *
     *
     */
    public function validate()
    {
        // 打順1~9が埋まっていること
        if ($this->starting_order->pluck('batting_order')->all() !== [1, 2, 3, 4, 5, 6, 7, 8, 9]) return false;

        // 捕手〜DHの全9ポジションを埋めていること
        $correct_positions = [Position::catcher, Position::first, Position::second, Position::third, Position::shortstop, Position::left, Position::center, Position::right, Position::designated_hitter];
        $positions = $this->starting_order->pluck('position')->all();
        if (!empty(array_diff($correct_positions, $positions)) || !empty(array_diff($positions, $correct_positions))) return false;

        // 先発投手が5人揃っていること
        if ($this->starter_pitchers->pluck('order')->all() !== [1, 2, 3, 4, 5]) return false;

        // 全てがplayer_idを持っていること
        if (count(array_filter($this->getLineupPlayerIds(), function($id) { return !is_numeric($id); })) !== 0) return false;

        // 控え野手が7人以下であること
        $batter_roles = Role::getBatterRoles();
        $bench_batters = $this->bench_player_roles->filter(function($item) use ($batter_roles) { return in_array($item->role, $batter_roles); });
        if ($bench_batters->count() > TeamOrder::BENCH_BATTER_COUNT) return false;

        // 控え投手が7人以下であること
        $pitcher_roles = Role::getPitcherRoles();
        $bench_pitchers = $this->bench_player_roles->filter(function($item) use ($pitcher_roles) { return in_array($item->role, $pitcher_roles); });
        if ($bench_pitchers->count() > TeamOrder::BENCH_PITCHER_COUNT) return false;

        // player_idに重複がないこと
        if (count(array_unique($this->getLineupPlayerIds())) !== count($this->getLineupPlayerIds())) return false;

        return true;
    }

    public function toJson()
    {
        return json_encode([
            'starting_order' => $this->starting_order->toArray(),
            'starter_pitchers' => $this->starter_pitchers->toArray(),
            'bench_player_roles' => $this->bench_player_roles->toArray()
        ]);
    }

    public function toArray()
    {
        return (Array)$this;
    }

}

