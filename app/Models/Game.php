<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public function home_team()
    {
        return $this->hasOne('App\Models\Team', 'id', 'home_team_id');
    }

    public function visitor_team()
    {
        return $this->hasOne('App\Models\Team', 'id', 'visitor_team_id')
                ->withDefault(function () {
                    $team = new \App\Models\Team();
                    $team->id = -1;
                    $team->name = '(CPUチーム)';
                    return $team;
                });
    }
}
