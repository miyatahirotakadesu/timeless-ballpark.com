<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerPitchingResult extends Model
{
    public function player()
    {
        return $this->belongsTo('App\Models\Player');
    }
}
