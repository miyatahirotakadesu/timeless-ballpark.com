<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{

    const GIFT_TYPE_COIN = 10;

    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }
}
