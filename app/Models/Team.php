<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{

    protected $appends = ['exp_percentage'];

    const NOUSER_TEAMNAME_PREFIX = '新チームNo.';
    const INITIAL_COIN = 1000;

    const LOGIN_BONUS_COIN = 100;
    const LOGIN_BONUS_EXP = 10;

    protected static function boot() {
        parent::boot();

        static::deleting(function($team) { // before delete() method call this
            $team->players()->detach();
            $team->mails()->delete();
            $team->team_orders()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /*
     * 試合出場可能な現役選手のみを返す
     * ControllerやLibなどから年齢を上げ下げする処理を行う時は、必ずretiredも連動して更新するように実装しなければならない
     */
    public function active_players()
    {
        return $this->belongsToMany('App\Models\Player', 'possessed_players')->withPivot('age', 'skip_level', 'once_skip_executed')->withTimestamps()->wherePivot('retired', 0);
    }

    public function players()
    {
        return $this->belongsToMany('App\Models\Player', 'possessed_players')->withPivot('age', 'skip_level', 'once_skip_executed')->withTimestamps();
    }

    public function mails()
    {
        return $this->hasMany('App\Models\Mail');
    }

    public function team_orders()
    {
        return $this->hasMany('App\Models\TeamOrder');
    }

    public function team_records()
    {
        return $this->hasMany('App\Models\TeamRecord');
    }

    public function introduced_assess_logs()
    {
        return $this->hasMany('App\Models\IntroducedAccessLog');
    }

    /*
     * ログイン実行
     * ログインボーナスが発生すれば更新
     * ログインボーナスが発生したかどうかをbool値で返す
     */
    public function execLogin()
    {
        $is_accrued_login_bonus = date('Y-m-d') !== date('Y-m-d', strtotime($this->logged_in_at));
        if ($is_accrued_login_bonus) {
            // ログインボーナス発生
            $this->coin += self::LOGIN_BONUS_COIN;
            $this->exp += self::LOGIN_BONUS_EXP;
            $this->updateLevel();
        }
        $this->logged_in_at = date('Y-m-d H:i:s');
        $this->save();
        return $is_accrued_login_bonus;
    }

    /*
     * 経験値からレベルを計算、更新する
     * [Lv]になるために必要な経験値＝ (30 * [lv])^1.2
     */
    public function updateLevel()
    {
        $level_tmp = pow($this->exp, 1/1.2) / 30;
        $this->level = floor($level_tmp);
    }

    /*
     * 次のレベルまで到達するために経験値が何%溜まっているか
     */
    public function getExpPercentageAttribute()
    {
        if ($this->id === -1) return false;

        $necessary_exp = pow(30 * ($this->level + 1), 1.2) - pow(30 * $this->level, 1.2);
        $now_filled_exp = $this->exp - pow(30 * $this->level, 1.2);
        return round($now_filled_exp / $necessary_exp, 2) * 100;
    }

}
