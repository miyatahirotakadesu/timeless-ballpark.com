<?php

namespace App\Console\Commands;

use DB;

use Goutte\Client;
use App\Enum\NpbTeam as NpbTeam;
use App\Models\Player as Player;
use App\Models\PlayerBattingResult as PlayerBattingResult;
use App\Models\PlayerPitchingResult as PlayerPitchingResult;
use App\Models\PlayerBattingCareerResult as PlayerBattingCareerResult;
use App\Models\PlayerPitchingCareerResult as PlayerPitchingCareerResult;

use Illuminate\Console\Command;

class CrawlWikiPlayers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl_wiki_players';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $INVESTIGATION_TEAMNAME_FILENAME;
    private $investigation_teamnames;

    private static $exception_url_players = [
        //'秋元肇' => '/wiki/秋元肇_(野球)',
    ];

    // 成績がないため登録しない選手たち
    private static $unregister_player_names = [
        '秋元肇', '深沢建彦', '深沢督', 'アレハンドロ・セゴビア', 'アデラーノ・リベラ', '吹田俊明'
    ];
    private static $allowed_batting_error_players = [

    ];
    private static $allowed_pitching_error_players = [
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 存在するチーム名調査
        $this->INVESTIGATION_TEAMNAME_FILENAME = storage_path('teamnames.txt');
        if (file_exists($this->INVESTIGATION_TEAMNAME_FILENAME)) {
            $this->investigation_teamnames = json_decode(file_get_contents($this->INVESTIGATION_TEAMNAME_FILENAME), true);
        } else {
            $this->investigation_teamnames = [];
        }

        $this->client = new Client();

        $this->client->setClient(new \GuzzleHttp\Client([
                \GuzzleHttp\RequestOptions::VERIFY => false,
        ]));
        /*
        $url = 'https://ja.wikipedia.org/wiki/%E6%97%A5%E6%9C%AC%E3%81%AE%E3%83%97%E3%83%AD%E9%87%8E%E7%90%83%E9%81%B8%E6%89%8B%E4%B8%80%E8%A6%A7';
        $crawler = $this->client->request('GET', $url);

        $player_list_tmp = $crawler->filter('h3 + ul')->each(function($ul, $index) {
            if ($index < 2 || $index > 84) return null; // 「あ」〜「わ」まで
            return $ul->filter('li a')->each(function($a, $index) {
                $name = $a->text();
                $url = (in_array($name, self::$exception_url_players)) ? self::$exception_url_players[$name] : $a->attr('href');
                if (substr($url, 0, 6) !== '/wiki/') return null;
                return [
                    'name' => $name,
                    'url' => $url
                ];
            });
        });

        $player_list = [];
        foreach ($player_list_tmp as $player_list_by_kana) {
            if (empty($player_list_by_kana)) continue;
            foreach ($player_list_by_kana as $player) {
                if (!empty($player)) $player_list[] = $player;
            }
        }

        $offset = 4408; // 途中から進めたい場合用
        $player_list = array_slice($player_list, $offset); // 途中から進めたい場合用
        $i = $offset;
        foreach ($player_list as $player) {
            if (in_array($player['name'], self::$unregister_player_names)) continue;
            var_dump($i . ':' . $player['name'] . '->' . $player['url']);
            $i++;
            $this->crawl_player($player);
            usleep(200000);
        }

        $team_urls = ['読売ジャイアンツの選手一覧', '阪神タイガースの選手一覧', '中日ドラゴンズの選手一覧', '横浜DeNAベイスターズの選手一覧', '広島東洋カープの選手一覧', '東京ヤクルトスワローズの選手一覧', 'オリックス・バファローズの選手一覧', '福岡ソフトバンクホークスの選手一覧', '北海道日本ハムファイターズの選手一覧', '千葉ロッテマリーンズの選手一覧', '埼玉西武ライオンズの選手一覧', '東北楽天ゴールデンイーグルスの選手一覧'];
        $player_list = [];
        foreach ($team_urls as $team_url_key => $team_url_str) {
            var_dump($team_url_str);
            $url = 'https://ja.wikipedia.org/wiki/' . urlencode($team_url_str);
            $crawler = $this->client->request('GET', $url);
            $player_name_td_val = (in_array($team_url_key, [1])) ? 1 : 0;

            $crawler->filter('h3 span.mw-headline')->each(function($h3, $index) use (&$player_list, $player_name_td_val) {
                if (in_array($h3->text(), ['投手', '捕手', '内野手', '外野手'])) {
                    return $h3->parents()->nextAll()->filter('table.wikitable')->first()->filter('tr')->each(function($tr, $tr_index) use (&$player_list, $player_name_td_val) {
                        if ($tr_index === 0) return null;
                        $name = $tr->filter('td')->eq($player_name_td_val)->filter('a')->text();
                        $url = $tr->filter('td')->eq($player_name_td_val)->filter('a')->first()->attr('href');
                        if (substr($url, 0, 6) !== '/wiki/') return null;
                        $player_list[] = [
                            'name' => $name,
                            'url' => $url
                        ];
                    });
                }
            });
        }

        $offset = 0; // 途中から進めたい場合用
        $player_list = array_slice($player_list, $offset); // 途中から進めたい場合用
        $i = $offset;
        foreach ($player_list as $player) {
            if (in_array($player['name'], self::$unregister_player_names)) continue;
            var_dump('現役' . $i . ':' . $player['name'] . '->' . $player['url']);
            $i++;
            $this->crawl_player($player);
            usleep(200000);
        }
        */

        $minus_rate_players = ['今久留主淳', '大沢喜好', '鍵谷康司', '高松延次', '千原雅生', '原一朗', '渡辺敬蔵', 'マティ・アルー', 'ブランドン・アレン', 'ウィルソン・バルデス', 'マット・ウェスト', 'エドウィン・エスコバー', 'ニック・エバンス', 'ブラッド・エルドレッド', 'ロス・オーレンドルフ', '片山文男', 'ビクター・ガラテ', 'ホセ・アドリス・ガルシア', 'フランク・カンポス', 'ゲーブ・キャプラー', 'マイク・キャンベル', 'セス・グライシンガー', 'ルルデス・グリエル', 'クレイグ・ワーシントン', 'フィル・コーク', 'ディッキー・ゴンザレス', 'アンソニー・サンダース', 'ブライアン・シャウス', 'デービー・ジョンソン', '申成鉉', 'ジェイソン・スタンリッジ', 'マット・ステアーズ', 'スティーブ・オンティベロス', 'マット・ダフィー', 'スコット・チャイアソン', 'ボブ・チャンス', '蔡森夫', 'アルヴィン・デービス', 'リック・デハート', 'スティーブ・デラバー', 'ジョナサン・ナナリー', 'マイク・バークベック', 'フランク・ハーマン', 'ジェレミー・ハーミダ', 'ラファエル・バティスタ', 'ジョーイ・バトラー', 'ブライアン・バニスター', 'デュアン・ビロウ', 'ジェイソン・フィリップス', 'ジェイソン・プライディ', 'ヤンシー・ブラゾバン', 'クリフ・ブランボー', 'ダン・ブリッグス', 'バーナード・ブリトー', 'ブラッド・ペニー', 'ラファエル・ペレス', 'ルディ・ペンバートン', 'ジェイソン・ボッツ', 'デーモン・ホリンズ', 'パット・マホームズ', 'ダン・ミセリ', 'セルジオ・ミトレ', 'ライル・ムートン', 'リッチ・モンテレオーネ', '余文彬', 'クリス・ラルー', 'クリス・リーソップ', '林彦峰', 'ランディ・ルイーズ', 'クリス・レイサム', '村田透'];
        foreach ($minus_rate_players as $name) {
            $player = [
                'name' => $name,
                'url' => '/wiki/' . urlencode($name)
            ];
            var_dump($player['name'] . '->' . $player['url']);
            $this->crawl_player($player);
            usleep(200000);
        }


    }

    private static $batting_table_map = [
        0 => 'year',
        1 => 'teamname',
        2 => 'game',
        3 => 'plate_appearance',
        4 => 'at_bats',
        5 => 'runs',
        6 => 'hits',
        7 => 'twobase',
        8 => 'triple',
        9 => 'homerun',
        10 => 'bases',
        11 => 'rbi',
        12 => 'stolen_base',
        13 => 'caught_stealing',
        14 => 'sacrifice_bunt',
        15 => 'sacrifice_fly',
        16 => 'fourball',
        17 => 'intentional_walk',
        18 => 'deadball',
        19 => 'strikeout',
        20 => 'doubleplay',
        21 => 'avg',
        22 => 'obp',
        23 => 'slg',
        24 => 'ops',
    ];
    private static $pitching_table_map = [
        0 => 'year',
        1 => 'teamname',
        2 => 'game',
        3 => 'starter',
        4 => 'complete_games',
        5 => 'shutouts',
        6 => 'no_bb_complete_games',
        7 => 'wins',
        8 => 'losses',
        9 => 'saves',
        10 => 'holds',
        11 => 'winning_percentage',
        12 => 'plate_appearance',
        13 => 'getout',
        14 => 'allowed_hits',
        15 => 'allowed_homeruns',
        16 => 'bases_on_balls',
        17 => 'intentional_bases_on_balls',
        18 => 'hit_by_pitches',
        19 => 'strikeouts',
        20 => 'wildpitch',
        21 => 'balk',
        22 => 'allowed_runs',
        23 => 'earned_runs',
        24 => 'era',
        25 => 'whip',
    ];
    private $player;
    private $player_batting_results;
    private $player_pitching_results;
    private $player_batting_career_result;
    private $player_pitching_career_result;

    private function crawl_player($player) {
        $url = 'https://ja.wikipedia.org' . $player['url'];
        $crawler = $this->client->request('GET', $url);

        // 公式戦出場なしの選手は記録しない
        $no_storeing_flag = false;
        $crawler->filter('ul li')->each(function($li) use (&$no_storeing_flag) {
            if (in_array($li->text(), ['一軍公式戦出場なし', '一軍公式戦出場無し'])) $no_storeing_flag = true;
        });
        if (count($crawler->filter('#年度別打撃成績')) === 0 && count($crawler->filter('#年度別投手成績')) === 0) $no_storeing_flag = true;
        if ($no_storeing_flag === true) return false;

        $this->player = $player;
        $this->player_batting_results = null;
        $this->player_pitching_results = null;
        $this->player_batting_career_result = null;
        $this->player_pitching_career_result = null;

        // 選手情報一覧取得
        $this->player['true_name'] = trim(preg_replace('/[0-9a-zA-Z]/', '', $crawler->filter('table.infobox caption')->text()));
        $player_infos_tmp = $crawler->filter('table.infobox tr th')->each(function($th, $i) {
            if (strpos($th->text(), '国籍') !== false) {
                $is_foreigner = strpos($th->nextAll()->filter('td')->first()->text(), '日本') === false;
                return ['key' => 'is_foreigner', 'value' => $is_foreigner];
            } else if (strpos($th->text(), '生年月日') !== false) {
                preg_match('/\d{4}年\d{1,2}月\d{1,2}日/', $th->nextAll()->filter('td')->first()->text(), $matches);
                if (count($matches) === 0) {
                    preg_match('/\d{4}年/', $th->nextAll()->filter('td')->first()->text(), $matches);
                    $matches[0] = $matches[0] . '4月1日';
                }
                return ['key' => 'bday', 'value' => \DateTime::createFromFormat('Y年m月d日', $matches[0])->format('Y-m-d')];
            } else if (strpos($th->text(), '投球・打席') !== false) {
                return ['key' => 'throw_bat', 'value' => $th->nextAll()->filter('td')->first()->text()];
            } else if (strpos($th->text(), 'ポジション') !== false) {
                return ['key' => 'positions', 'value' => $th->nextAll()->filter('td')->first()->text()];
            }
        });
        foreach ($player_infos_tmp as $arr) {
            if ($arr === null) continue;
            $this->player[$arr['key']] = $arr['value'];
        }
        if (!array_key_exists('is_foreigner', $this->player)) $this->player['is_foreigner'] = 0;
        if (!array_key_exists('throw_bat', $this->player)) $this->player['throw_bat'] = '右投右打';
        if (mb_strlen($this->player['throw_bat']) !== 4) $this->player['throw_bat'] = '右投右打';
        $throw_bat_val = ['右' => 'R', '左' => 'L', '両' => 'S'];
        $this->player['throw'] = $throw_bat_val[mb_substr($this->player['throw_bat'], 0, 1)];
        $this->player['bat'] = $throw_bat_val[mb_substr($this->player['throw_bat'], 2, 1)];

        // 誕生日
        $birth_year_tmp = \DateTime::createFromFormat('Y-m-d', $this->player['bday'])->format('Y');
        $birth_month = \DateTime::createFromFormat('Y-m-d', $this->player['bday'])->format('m');
        $this->player['born_year'] = ($birth_month < 4) ? $birth_year_tmp - 1 : $birth_year_tmp; //　早生まれを考慮して何年度生まれかを取得

        //経歴から入団年を取得
        $is_keireki_tr = false;
        $joined_year = null;
        $crawler->filter('table.infobox tr')->each(function($tr, $i) use (&$is_keireki_tr, &$joined_year) {
            if ($is_keireki_tr === false && strpos($tr->text(), '経歴') !== false) {
                // 経歴の説明が始まったということを表す
                $is_keireki_tr = true;
            } else if ($is_keireki_tr === true && $tr->attr('itemprop') === null) {
                // もう経歴の欄は終わったということを表す
                $is_keireki_tr = false;
            }

            // 経歴の説明中。一番小さい４桁の数字が入団年と判断させる
            if ($is_keireki_tr === true) {
                preg_match_all('/\d{4}/', $tr->text(), $matches);
                foreach ($matches as $m) {
                    foreach ($m as $degit) {
                        if ($joined_year === null || $joined_year > (int)$degit) $joined_year = (int)$degit;
                    }
                }
            }
        });
        $this->player['joined_year'] = $joined_year;
        $this->player['joined_age'] = $this->player['joined_year'] - $this->player['born_year'];

        $this->player_batting_results = []; // リセット
        $this->player_pitching_results = []; // リセット
        $this->read_wiki_result_table($crawler, 'batting');
        $this->read_wiki_result_table($crawler, 'pitching');

        // 打撃成績＆投手成績から引退年を推定し保存
        $this->player['retired_year'] = (int)collect($this->player_pitching_results)->pluck('year')->max();
        $this->player['retired_age'] = $this->player['retired_year'] - $this->player['born_year'];

        $this->save_process();

    }

    private function read_wiki_result_table($crawler, $batting_or_pitching) {

        $result_span = ($batting_or_pitching === 'batting') ? $crawler->filter('#年度別打撃成績') : $crawler->filter('#年度別投手成績');
        if (count($result_span) > 0) {
            $trs = $result_span->parents()->nextAll()->filter('table.wikitable')->eq(0)->filter('tr');
            $tr_count = count($trs);
            $transfered_flag = false;
            $teamname = null;
            $trs->each(function($tr, $index) use ($batting_or_pitching, $tr_count, &$transfered_flag, &$teamname) {
                if ($index === 0) {
                    // 1行目 タイトル行
                    if ($batting_or_pitching === 'batting' && !in_array($this->player['name'], self::$allowed_batting_error_players) && count($tr->filter('th')) !== 25) dd($this->player['name'] . ' の打撃成績が25列じゃありません');
                    if ($batting_or_pitching === 'pitching' && !in_array($this->player['name'], self::$allowed_pitching_error_players) && count($tr->filter('th')) !== 26) dd($this->player['name'] . ' の投手成績が26列じゃありません');
                } else {

                    $transfered_flag = $transfered_flag || count($tr->filter('td')->eq(0)->filter('[rowspan]')) > 0;
                    $multiteam_one_season_sum_flag = false; // 移籍があった年の○○年の複数チーム合計成績を表す列かどうか
                    if ($transfered_flag === true) {
                        // シーズン途中に移籍があったことがわかった以降の列
                        // シーズン中移籍があった場合はどちらのチームの成績も保存せず、合計成績のみを保存するようにする
                        if (count($tr->filter('th')) > 0) {
                            // シーズン両チームの合計成績の列の場合
                            $transfered_flag = false;
                            $multiteam_one_season_sum_flag = true;
                        } else {
                            // 移籍先チームの単年成績
                            return null;
                        }
                    } else if (count($tr->filter('th')) > 0) {
                        // 久保康生選手のように「一番左のtdにrowspan」じゃない形でのシーズン移籍パターンもある
                        $multiteam_one_season_sum_flag = true;
                    }

                    $is_career = count($tr->filter('th[colspan=2]')) > 0;
                    $is_hatsuoka = $this->player['name'] === '初岡栄治'; // 通算成績のthのところにrowspanがない人
                    if ($is_hatsuoka && $tr_count === $index + 1) {
                        $is_career = true; // 初岡栄治のページは例外
                    }

                    if ($is_career === false) {
                        // 通常の各年度成績

                        $player_oneseason_result = [];

                        $newteam_td_count = ($batting_or_pitching === 'batting') ? 25 : 26;
                        $is_newteam_tr = count($tr->filter('td')) === $newteam_td_count; // プロ１年目 or 移籍初年度ならtrue
                        if ($is_newteam_tr === true) {
                            $a = $tr->filter('td')->eq(1)->filter('a');
                            if (count($a) > 0) {
                                $href = $tr->filter('td')->eq(1)->filter('a')->attr('href');
                                $teamname = str_replace('/wiki/', '', urldecode($href));
                                $this->add_teamname_by_investigation($teamname); // チーム名調査のため
                            } else {
                                // チームのところがリンク切れしている時
                                $teamname = 'WHITE';
                                \Log::info($this->player['name'] . 'のチーム名のリンク切れ発見');
                            }
                        }

                        // wikiの成績テーブルを横に見ていき安打、本塁打・・・と順に$player_oneseason_resultに記録
                        $tr->filter('td')->each(function($td, $td_index) use ($batting_or_pitching, $is_newteam_tr, $multiteam_one_season_sum_flag, &$player_oneseason_result) {
                            $map_key = ($is_newteam_tr === false && $td_index >= 1) ? $td_index + 1 : $td_index;
                            if ($multiteam_one_season_sum_flag === true) $map_key = $td_index + 2;

                            $column_name = ($batting_or_pitching === 'batting') ? self::$batting_table_map[$map_key] : self::$pitching_table_map[$map_key];
                            $player_oneseason_result[$column_name] = ($batting_or_pitching === 'batting') ? $this->format_batting_result_value($column_name, $td->text()) : $this->format_pitching_result_value($column_name, $td->text());
                        });
                        // '99計とかの１シーズンの複数チーム成績合計の場合、yearを別途取得
                        if ($multiteam_one_season_sum_flag === true) {
                            preg_match('/\d{2}/', $tr->filter('th')->first()->text(), $matches);
                            $year = (int)$matches[0];
                            $player_oneseason_result['year'] = ($year < 17) ? 2000 + $year : 1900 + $year;
                        }

                        if (!array_key_exists('teamname', $player_oneseason_result)) $player_oneseason_result['teamname'] = $teamname;
                        $player_oneseason_result['belong_to'] = NpbTeam::getNpbTeamId($teamname);

                        // 所属球団がよくわからん不明な球団なら成績をDBに保存しない
                        if ($player_oneseason_result['belong_to'] !== NpbTeam::TEAM_OTHER) {
                            if ($batting_or_pitching === 'batting') {
                                $this->player_batting_results[] = $player_oneseason_result;
                            } else {
                                $this->player_pitching_results[] = $player_oneseason_result;
                            }
                        }
                    } else {
                        // 通算成績の場合
                        // NPB+MLB選手などの通算成績は合算とする

                        // 韓国や中国リーグの成績・外国人選手のMLBでの成績は含まない
                        if (!$is_hatsuoka) {
                            $is_kbo = strpos($tr->filter('th[colspan=2]')->first()->text(), 'KBO') !== false;
                            $is_cpbl = strpos($tr->filter('th[colspan=2]')->first()->text(), 'CPBL') !== false;
                            $is_mlb = strpos($tr->filter('th[colspan=2]')->first()->text(), 'MLB') !== false;
                            if ($is_kbo || $is_cpbl || $this->player['is_foreigner'] === true && $is_mlb) return false;
                        }

                        // 通算成績を合算していく(主にイチローとかのメジャーリーガーの成績を合算するため）
                        $tr->filter('td')->each(function($td, $td_index) use ($batting_or_pitching, $is_hatsuoka) {
                            $map_key = (!$is_hatsuoka) ? $td_index + 2 : $td_index + 1;
                            $column_name = ($batting_or_pitching === 'batting') ? self::$batting_table_map[$map_key] : self::$pitching_table_map[$map_key];

                            if ($batting_or_pitching === 'batting') {
                                if (isset($this->player_batting_career_result[$column_name])) {
                                    $this->player_batting_career_result[$column_name] += $this->format_batting_result_value($column_name, $td->text());
                                } else {
                                    $this->player_batting_career_result[$column_name] = $this->format_batting_result_value($column_name, $td->text());
                                }
                            } else {
                                if (isset($this->player_pitching_career_result[$column_name])) {
                                    $this->player_pitching_career_result[$column_name] += $this->format_pitching_result_value($column_name, $td->text());
                                } else {
                                    $this->player_pitching_career_result[$column_name] = $this->format_pitching_result_value($column_name, $td->text());
                                }
                            }
                        });

                        if ($tr_count === $index + 1) {
                            // 最終行の場合通算成績を計算し保存
                            if ($batting_or_pitching === 'batting') {
                                $this->player_batting_career_result = $this->format_batting_career_result($this->player_batting_career_result);
                            } else {
                                $this->player_pitching_career_result = $this->format_pitching_career_result($this->player_pitching_career_result);
                            }
                        }
                    }
                }
            });

        }
    }

    // 打率などをDBに保存するための形式に変換する
    private function format_batting_result_value($column_name, $value)
    {
        switch($column_name) {
            case 'avg':
            case 'obp':
            case 'slg':
            case 'ops':
                return ($value < 1) ? '0' . $value : $value;
                break;
            default:
                return $value;
        }
    }

    // 投球回などをDBに保存するための形式に変換する
    private function format_pitching_result_value($column_name, $value)
    {
        switch($column_name) {
            case 'getout':
                $getouts = explode('.', $value);
                $syousu = (isset($getouts[1])) ? $getouts[1] : 0;
                $getout = $getouts[0] * 3 + $syousu;
                return $getout;
                break;
            default:
                return $value;
        }
    }

    // 通算成績の打率などを正しく計算
    private function format_batting_career_result($batting_career_result)
    {
        $at_bats = ($batting_career_result['at_bats'] > 0) ? $batting_career_result['at_bats'] : 1;
        $syuturui = $batting_career_result['hits'] + $batting_career_result['fourball'] + $batting_career_result['deadball'];
        $obp_daseki = $at_bats + $batting_career_result['fourball'] + $batting_career_result['deadball'] + $batting_career_result['sacrifice_fly'];
        $batting_career_result['avg'] = round($batting_career_result['hits'] / $at_bats, 3);
        $batting_career_result['obp'] = $syuturui / $obp_daseki;
        $batting_career_result['slg'] = $batting_career_result['bases'] / $at_bats;
        $batting_career_result['ops'] = round($batting_career_result['obp'] + $batting_career_result['slg'], 3);

        $batting_career_result['obp'] = round($batting_career_result['obp'], 3);
        $batting_career_result['slg'] = round($batting_career_result['slg'], 3);

        return $batting_career_result;
    }

    // 通算成績の防御率などを正しく計算
    private function format_pitching_career_result($pitching_career_result)
    {
        $getout = (empty($pitching_career_result['getout'])) ? 1 : $pitching_career_result['getout'];
        $pitching_career_result['era'] = round(27 * $pitching_career_result['earned_runs'] / $getout, 2);
        $pitching_career_result['whip'] = round(3 * ($pitching_career_result['allowed_hits'] + $pitching_career_result['bases_on_balls']) / $getout, 2);
        return $pitching_career_result;
    }

    // DBへの保存
    private function save_process()
    {
        // Playerを保存
        $player = Player::where('name', $this->player['true_name'])->first();
        if ($player === null) $player = new Player();
        $player->name = $this->player['true_name'];
        $player->rate = -1;
        $player->positions = $this->player['positions'];
        $player->is_foreigner = $this->player['is_foreigner'];
        $player->born_year = $this->player['born_year'];
        $player->joined_year = $this->player['joined_year'];
        $player->retired_year = $this->player['retired_year'];
        $player->joined_age = $this->player['joined_age'];
        $player->retired_age = $this->player['retired_age'];
        $player->throw = $this->player['throw'];
        $player->bat = $this->player['bat'];
        $player->save();

        $player_id = $player->id;

        if (isset($this->player_pitching_results, $this->player_pitching_career_result)) {
            // 単年投手成績
            foreach ($this->player_pitching_results as $ppr) {
                $player_pitching_result = PlayerPitchingResult::where('player_id', $player_id)->where('year', $ppr['year'])->first();
                if ($player_pitching_result === null) $player_pitching_result = new PlayerPitchingResult();
                $player_pitching_result->player_id = $player_id;
                $player_pitching_result->year = (int)$ppr['year'];
                $player_pitching_result->player_age = (int)($ppr['year'] - $player->born_year);
                $player_pitching_result->belong_to = (int)$ppr['belong_to'];
                $player_pitching_result->game = (int)$ppr['game'];
                $player_pitching_result->starter = (int)$ppr['starter'];
                $player_pitching_result->plate_appearance = (int)$ppr['plate_appearance'];
                $player_pitching_result->getout = (int)$ppr['getout'];
                $player_pitching_result->allowed_runs = (int)$ppr['allowed_runs'];
                $player_pitching_result->earned_runs = (int)$ppr['earned_runs'];
                $player_pitching_result->allowed_hits = (int)$ppr['allowed_hits'];
                $player_pitching_result->allowed_homeruns = (int)$ppr['allowed_homeruns'];
                $player_pitching_result->strikeouts = (int)$ppr['strikeouts'];
                $player_pitching_result->bases_on_balls = (int)$ppr['bases_on_balls'];
                $player_pitching_result->intentional_bases_on_balls = (int)$ppr['intentional_bases_on_balls'];
                $player_pitching_result->hit_by_pitches = (int)$ppr['hit_by_pitches'];
                $player_pitching_result->wins = (int)$ppr['wins'];
                $player_pitching_result->losses = (int)$ppr['losses'];
                $player_pitching_result->holds = (int)$ppr['holds'];
                $player_pitching_result->saves = (int)$ppr['saves'];
                $player_pitching_result->complete_games = (int)$ppr['complete_games'];
                $player_pitching_result->shutouts = (int)$ppr['shutouts'];
                $player_pitching_result->whip = (float)$ppr['whip'];
                $player_pitching_result->era = (float)$ppr['era'];
                $player_pitching_result->save();
            }

            // 通算投手成績
            $player_pitching_career_result = PlayerPitchingCareerResult::where('player_id', $player_id)->first();
            if ($player_pitching_career_result === null) $player_pitching_career_result = new PlayerPitchingCareerResult();
            $ppcr = $this->player_pitching_career_result;
            $player_pitching_career_result->player_id = $player_id;
            $player_pitching_career_result->game = (int)$ppcr['game'];
            $player_pitching_career_result->starter = (int)$ppcr['starter'];
            $player_pitching_career_result->plate_appearance = (int)$ppcr['plate_appearance'];
            $player_pitching_career_result->getout = (int)$ppcr['getout'];
            $player_pitching_career_result->allowed_runs = (int)$ppcr['allowed_runs'];
            $player_pitching_career_result->earned_runs = (int)$ppcr['earned_runs'];
            $player_pitching_career_result->allowed_hits = (int)$ppcr['allowed_hits'];
            $player_pitching_career_result->allowed_homeruns = (int)$ppcr['allowed_homeruns'];
            $player_pitching_career_result->strikeouts = (int)$ppcr['strikeouts'];
            $player_pitching_career_result->bases_on_balls = (int)$ppcr['bases_on_balls'];
            $player_pitching_career_result->intentional_bases_on_balls = (int)$ppcr['intentional_bases_on_balls'];
            $player_pitching_career_result->hit_by_pitches = (int)$ppcr['hit_by_pitches'];
            $player_pitching_career_result->wins = (int)$ppcr['wins'];
            $player_pitching_career_result->losses = (int)$ppcr['losses'];
            $player_pitching_career_result->holds = (int)(int)$ppcr['holds'];
            $player_pitching_career_result->saves = (int)$ppcr['saves'];
            $player_pitching_career_result->complete_games = (int)$ppcr['complete_games'];
            $player_pitching_career_result->shutouts = (int)$ppcr['shutouts'];
            $player_pitching_career_result->whip = (float)$ppcr['whip'];
            $player_pitching_career_result->era = (float)$ppcr['era'];
            $player_pitching_career_result->save();
        }

        if (isset($this->player_batting_results, $this->player_batting_career_result)) {
            // 単年打撃成績
            foreach ($this->player_batting_results as $pbr) {
                $player_batting_result = PlayerBattingResult::where('player_id', $player_id)->where('year', $pbr['year'])->first();
                if ($player_batting_result === null) $player_batting_result = new PlayerBattingResult();
                $player_batting_result->player_id = $player_id;
                $player_batting_result->year = (int)$pbr['year'];
                $player_batting_result->player_age = (int)($pbr['year'] - $player->born_year);
                $player_batting_result->belong_to = (int)$pbr['belong_to'];
                $player_batting_result->game = (int)$pbr['game'];
                $player_batting_result->plate_appearance = (int)$pbr['plate_appearance'];
                $player_batting_result->at_bats = (int)$pbr['at_bats'];
                $player_batting_result->runs = (int)$pbr['runs'];
                $player_batting_result->hits = (int)$pbr['hits'];
                $player_batting_result->twobase = (int)$pbr['twobase'];
                $player_batting_result->triple = (int)$pbr['triple'];
                $player_batting_result->homerun = (int)$pbr['homerun'];
                $player_batting_result->rbi = (int)$pbr['rbi'];
                $player_batting_result->fourball = (int)$pbr['fourball'];
                $player_batting_result->intentional_walk = (int)$pbr['intentional_walk'];
                $player_batting_result->deadball = (int)$pbr['deadball'];
                $player_batting_result->strikeout = (int)$pbr['strikeout'];
                $player_batting_result->sacrifice_bunt = (int)$pbr['sacrifice_bunt'];
                $player_batting_result->sacrifice_fly = (int)$pbr['sacrifice_fly'];
                $player_batting_result->stolen_base = (int)$pbr['stolen_base'];
                $player_batting_result->caught_stealing = (int)$pbr['caught_stealing'];
                $player_batting_result->doubleplay = (int)$pbr['doubleplay'];
                $player_batting_result->avg = (float)$pbr['avg'];
                $player_batting_result->obp = (float)$pbr['obp'];
                $player_batting_result->slg = (float)$pbr['slg'];
                $player_batting_result->ops = (float)$pbr['ops'];
                // wikiの小数点抜けミス対策
                if ($player_batting_result->avg > 100) $player_batting_result->avg = $player_batting_result->avg / 1000;
                if ($player_batting_result->obp > 100) $player_batting_result->obp = $player_batting_result->obp / 1000;
                if ($player_batting_result->slg > 100) $player_batting_result->slg = $player_batting_result->slg / 1000;
                if ($player_batting_result->ops > 100) $player_batting_result->ops = $player_batting_result->ops / 1000;
                $player_batting_result->save();
            }

            // 通算打撃成績
            $player_batting_career_result = PlayerBattingCareerResult::where('player_id', $player_id)->first();
            if ($player_batting_career_result === null) $player_batting_career_result = new PlayerBattingCareerResult();
            $pbcr = $this->player_batting_career_result;
            $player_batting_career_result->player_id = $player_id;
            $player_batting_career_result->game = (int)$pbcr['game'];
            $player_batting_career_result->plate_appearance = (int)$pbcr['plate_appearance'];
            $player_batting_career_result->at_bats = (int)$pbcr['at_bats'];
            $player_batting_career_result->runs = (int)$pbcr['runs'];
            $player_batting_career_result->hits = (int)$pbcr['hits'];
            $player_batting_career_result->twobase = (int)$pbcr['twobase'];
            $player_batting_career_result->triple = (int)$pbcr['triple'];
            $player_batting_career_result->homerun = (int)$pbcr['homerun'];
            $player_batting_career_result->rbi = (int)$pbcr['rbi'];
            $player_batting_career_result->fourball = (int)$pbcr['fourball'];
            $player_batting_career_result->intentional_walk = (int)$pbcr['intentional_walk'];
            $player_batting_career_result->deadball = (int)$pbcr['deadball'];
            $player_batting_career_result->strikeout = (int)$pbcr['strikeout'];
            $player_batting_career_result->sacrifice_bunt = (int)$pbcr['sacrifice_bunt'];
            $player_batting_career_result->sacrifice_fly = (int)$pbcr['sacrifice_fly'];
            $player_batting_career_result->stolen_base = (int)$pbcr['stolen_base'];
            $player_batting_career_result->caught_stealing = (int)$pbcr['caught_stealing'];
            $player_batting_career_result->doubleplay = (int)$pbcr['doubleplay'];
            $player_batting_career_result->avg = (float)$pbcr['avg'];
            $player_batting_career_result->obp = (float)$pbcr['obp'];
            $player_batting_career_result->slg = (float)$pbcr['slg'];
            $player_batting_career_result->ops = (float)$pbcr['ops'];
            $player_batting_career_result->save();
        }

    }

    private function add_teamname_by_investigation($teamname)
    {
        $this->investigation_teamnames[] = $teamname;
        $this->investigation_teamnames = array_values(array_unique($this->investigation_teamnames));
        $text = mb_convert_encoding(json_encode($this->investigation_teamnames, JSON_UNESCAPED_UNICODE), "UTF-8", "auto");
        return file_put_contents($this->INVESTIGATION_TEAMNAME_FILENAME, $text);
    }
}
