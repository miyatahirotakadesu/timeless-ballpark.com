<?php

namespace App\Console\Commands;

use DB;

use Goutte\Client;

use App\Models\Player as Player;
use App\Models\PlayerBattingResult as PlayerBattingResult;
use App\Models\PlayerBattingCareerResult as PlayerBattingCareerResult;
use App\Models\PlayerPitchingResult as PlayerPitchingResult;
use App\Models\PlayerPitchingCareerResult as PlayerPitchingCareerResult;

use Illuminate\Console\Command;

class DeleteUnpopularPlayers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete_unpopular_players {do?}';

    /**
     * The console command description.
     *
     * @var string
     */

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 全選手を取得する
        $players = Player::with(['player_batting_career_result', 'player_pitching_career_result'])->get();

        // 削除予定選手リスト作成
        $b = 0;
        $p = 0;
        $deleting_players = $players->filter(function($player) use (&$b, &$p) {

            if ($player->joined_year < 1960) {
                $criterias = [
                    'batter_game' => 1000,
                    'batter_ap' => 1000,
                    'pitcher_game' => 330,
                    'pitcher_ap' => 1000,
                ];
            } else if ($player->joined_year < 1990) {
                $criterias = [
                    'batter_game' => 900,
                    'batter_ap' => 900,
                    'pitcher_game' => 300,
                    'pitcher_ap' => 900,
                ];
            } else if ($player->joined_year < 2014) {
                $criterias = [
                    'batter_game' => 600,
                    'batter_ap' => 600,
                    'pitcher_game' => 200,
                    'pitcher_ap' => 600,
                ];
            } else {
                return false;
            }

            if (isset($player->player_batting_career_result)) {
                if ($player->player_batting_career_result->game >= $criterias['batter_game']) { $b++; return false; }
                if ($player->player_batting_career_result->plate_appearance >= $criterias['batter_ap']) { $b++; return false; }
            }

            if (isset($player->player_pitching_career_result)) {
                if ($player->player_pitching_career_result->game >= $criterias['pitcher_game']) { $p++; return false; }
                if ($player->player_pitching_career_result->plate_appearance >= $criterias['pitcher_ap']) { $p++; return false; }
            }

            return true;

        });

        var_dump('野手生き残り:' . $b . '名');
        var_dump('投手生き残り:' . $p . '名');
        var_dump($deleting_players->count());
        if ($this->argument('do') !== 'do') exit;

        // 削除実施
        foreach ($deleting_players as $deleting_player) {
            PlayerBattingResult::where('player_id', $deleting_player->id)->delete();
            PlayerBattingCareerResult::where('player_id', $deleting_player->id)->delete();
            PlayerPitchingResult::where('player_id', $deleting_player->id)->delete();
            PlayerPitchingCareerResult::where('player_id', $deleting_player->id)->delete();
            $deleting_player->delete();
        }

        // playersのid振り直し
        $players = Player::with(['player_batting_career_result', 'player_pitching_career_result'])->get();
        $player_id = 0;
        foreach ($players as $player) {
            $player_id++;
            $orig_player_id = $player->id;
            PlayerBattingResult::where('player_id', $orig_player_id)->update(['player_id' => $player_id]);
            PlayerBattingCareerResult::where('player_id', $orig_player_id)->update(['player_id' => $player_id]);
            PlayerPitchingResult::where('player_id', $orig_player_id)->update(['player_id' => $player_id]);
            PlayerPitchingCareerResult::where('player_id', $orig_player_id)->update(['player_id' => $player_id]);
            $player->id = $player_id;
            $player->save();
        }

    }

}

