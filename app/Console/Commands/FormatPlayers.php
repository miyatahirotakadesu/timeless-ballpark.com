<?php

namespace App\Console\Commands;

use DB;

use Goutte\Client;
use App\Enum\NpbTeam as NpbTeam;
use App\Models\Player as Player;
use App\Models\PlayerBattingResult as PlayerBattingResult;
use App\Models\PlayerPitchingResult as PlayerPitchingResult;
use App\Models\PlayerBattingCareerResult as PlayerBattingCareerResult;
use App\Models\PlayerPitchingCareerResult as PlayerPitchingCareerResult;

use Illuminate\Console\Command;

class FormatPlayers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'format_players';

    /**
     * The console command description.
     *
     * @var string
     */

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$this->format_positions();
        //$this->rate_players();
        //$this->modify_retired_year();
        $this->modify_foreinger_joined_year();
    }

    private function format_positions()
    {
        /*
        DB::update('update players set positions = REPLACE(positions, " ", "、");');
        DB::update('update players set positions = REPLACE(positions, "、指名打者", "");');
        DB::update('update players set positions = REPLACE(positions, "・", "、");');
        DB::update('update players set positions = REPLACE(positions, "[", "");');
        DB::update('update players set positions = REPLACE(positions, "]", "");');
        DB::update('update players set positions = REPLACE(positions, "1", "");');
        DB::update('update players set positions = REPLACE(positions, "、、", "、");');
         */
        $players = Player::all();
        foreach ($players as $player) {
            $positions = [];
            if (json_decode($player->positions) !== null) continue;
            $position_labels = explode('、', $player->positions);
            foreach ($position_labels as $position_label) {
                switch (trim($position_label)) {
                    case "投手":
                        $positions[] = 'pitcher';break;
                    case "捕手":
                        $positions[] = 'catcher';break;
                    case "内野手":
                        $positions[] = 'first';$positions[] = 'second';
                        $positions[] = 'third';$positions[] = 'shortstop'; break;
                    case "外野手":
                        $positions[] = 'left';$positions[] = 'center';$positions[] = 'right';break;
                    case "一塁手":
                        $positions[] = 'first';break;
                    case "三塁手":
                        $positions[] = 'third';break;
                    case "遊撃手":
                        $positions[] = 'shortstop';break;
                    case "二塁手":
                        $positions[] = 'second';break;
                    case "中堅手":
                        $positions[] = 'center';break;
                    case "右翼手":
                        $positions[] = 'right';break;
                    case "左翼手":
                        $positions[] = 'left';break;
                    default:
                        dd('error' . $player->name . ' / ' . $position_label);
                }
                if (count($positions) === 0) dd('error' . $player->name);
                $player->positions = json_encode($positions);
                $player->save();
            }
        }
    }

    private function rate_players()
    {
        $players = Player::with([
                       'player_batting_career_result' => function($query){ $query->select('id', 'player_id', 'hits', 'ops'); },
                       'player_pitching_career_result' => function($query){ $query->select('id','player_id', 'game', 'getout', 'strikeouts', 'allowed_homeruns', 'bases_on_balls', 'hit_by_pitches'); }
                   ])
                   ->get()->toArray();

        $batter_scores = [];
        $pitcher_scores = [];

        foreach ($players as $player) {
            if (isset($player['player_batting_career_result'])) {
                $batter_scores[] = $player['player_batting_career_result']['hits'] * $player['player_batting_career_result']['ops'];
            }
            if (isset($player['player_pitching_career_result'])) {
                $pitcher_scores[] = $player['player_pitching_career_result']['game'] * 3 + $player['player_pitching_career_result']['getout'];
            }
        } 

        $batter_scores_avg = array_sum($batter_scores) / count($batter_scores);
        $batter_pow_diff_by_avgs = array_map(function($val) use ($batter_scores_avg) { return ($val - $batter_scores_avg) * ($val - $batter_scores_avg); }, $batter_scores); // 平均との差を２乗した値の配列
        $batter_scores_dev = sqrt(array_sum($batter_pow_diff_by_avgs) / count($batter_pow_diff_by_avgs)); // 標準偏差

        $pitcher_scores_avg = array_sum($pitcher_scores) / count($pitcher_scores);
        $pitcher_pow_diff_by_avgs = array_map(function($val) use ($pitcher_scores_avg) { return ($val - $pitcher_scores_avg) * ($val - $pitcher_scores_avg); }, $pitcher_scores); // 平均との差を２乗した値の配列
        $pitcher_scores_dev = sqrt(array_sum($pitcher_pow_diff_by_avgs) / count($pitcher_pow_diff_by_avgs)); // 標準偏差

        foreach ($players as &$player) {
            if (isset($player['player_batting_career_result'])) {
                $score = $player['player_batting_career_result']['hits']* $player['player_batting_career_result']['ops'];
                $player['dev_val'] = 50 + round((($score - $batter_scores_avg)/ $batter_scores_dev) * 10, 3);
            }
            if (isset($player['player_pitching_career_result'])) {
                $score = $player['player_pitching_career_result']['game'] * 3 + $player['player_pitching_career_result']['getout'] + $player['player_pitching_career_result']['strikeouts'] * 0.2 - $player['player_pitching_career_result']['allowed_homeruns'] * 1.3 - $player['player_pitching_career_result']['bases_on_balls'] * 0.3 - $player['player_pitching_career_result']['hit_by_pitches'] * 0.3;
                $pitcher_tmp_dev_val = 50 + round((($score - $pitcher_scores_avg) / $pitcher_scores_dev) * 10, 3);
                if (empty($player['dev_val']) || $player['dev_val'] < $pitcher_tmp_dev_val) {
                    $player['dev_val'] = $pitcher_tmp_dev_val;
                }
            }

            if (isset($player['dev_val'])) {
                $update_player = Player::where('id', $player['id'])->first();
                if ($player['dev_val'] > 72) { $rate = 10; }
                else if ($player['dev_val'] > 63) { $rate = 9; }
                else if ($player['dev_val'] > 57) { $rate = 8; }
                else if ($player['dev_val'] > 53) { $rate = 7; }
                else if ($player['dev_val'] > 50) { $rate = 6; }
                else if ($player['dev_val'] > 47.5) { $rate = 5; }
                else if ($player['dev_val'] > 45) { $rate = 4; }
                else if ($player['dev_val'] > 43.5) { $rate = 3; }
                else if ($player['dev_val'] > 42) { $rate = 2; }
                else  { $rate = 1; }
                $update_player->rate = $rate;
                $update_player->save();
            } else {
                var_dump($player['name'] . 'の偏差値がない');
            }
        } 


    }

    private function modify_retired_year()
    {
        $players = Player::with([
                       'player_batting_results' => function($query){ $query->select('id', 'player_id', 'year'); },
                       'player_pitching_results' => function($query){ $query->select('id','player_id', 'year'); }
                   ])
                   ->get();
        foreach ($players as $player) {
            $retired_year = collect([$player->player_batting_results->max('year'), $player->player_pitching_results->max('year')])->max();
            if (isset($retired_year)) {
                $player->retired_year = $retired_year;
                $player->retired_age = $player->retired_year - $player->born_year;
                $player->save();
            } else {
                var_dump($player->name . 'の引退年不明 rate:' . $player->rate);
            }

        }
    }

    private function modify_foreinger_joined_year()
    {
        $players = Player::with([
                       'player_batting_results' => function($query){ $query->select('id', 'player_id', 'year'); },
                       'player_pitching_results' => function($query){ $query->select('id','player_id', 'year'); }
                   ])
                   ->where('is_foreigner', 1)
                   ->get();
        foreach ($players as $player) {
            $joined_year = collect([$player->player_batting_results->min('year'), $player->player_pitching_results->min('year')])->min();
            if (isset($joined_year)) {
                $orig_joined_year = $player->joined_year;
                $player->joined_year = $joined_year;
                $player->joined_age = $player->joined_year - $player->born_year;
                $player->save();
                if ($orig_joined_year !== $player->joined_year) var_dump($player->name . 'の入団年度' . $orig_joined_year . '->' . $player->joined_year);
            }

        }
    }

}

