<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->integer('game_number');
            $table->boolean('executed')->default(false);
            $table->integer('home_team_id');
            $table->integer('visitor_team_id');
            $table->integer('home_team_score')->nullable()->default(null);
            $table->integer('visitor_team_score')->nullable()->default(null);
            $table->integer('home_team_starter_player_id')->nullable()->default(null);
            $table->integer('visitor_team_starter_player_id')->nullable()->default(null);
            $table->boolean('walkoff_game_flg')->nullable()->default(null);
            $table->mediumText('content')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
