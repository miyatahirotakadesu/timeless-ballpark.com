<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->string('name', 32);
            $table->integer('rate');
            $table->string('positions', 128);
            $table->boolean('is_foreigner');
            $table->integer('born_year');
            $table->integer('joined_year');
            $table->integer('retired_year');
            $table->integer('joined_age');
            $table->integer('retired_age');
            $table->string('throw', 4);
            $table->string('bat', 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
