<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->nullable()->default(null);
            $table->string('session_id', 64)->nullable()->default(null);
            $table->string('name', 32);
            $table->boolean('changed_name')->default(false);
            $table->integer('coin')->default(0);
            $table->integer('win')->default(0);
            $table->integer('lose')->default(0);
            $table->integer('draw')->default(0);
            $table->integer('consecutive_wins')->default(0);
            $table->integer('rank')->default(0)->nullable();
            $table->integer('exp')->default(1);
            $table->integer('level')->default(1);
            $table->integer('total_win')->default(0);
            $table->integer('total_lose')->default(0);
            $table->integer('total_draw')->default(0);
            $table->dateTime('logged_in_at')->nullable();
            $table->timestamps();
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
