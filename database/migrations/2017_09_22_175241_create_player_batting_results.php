<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerBattingResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_batting_results', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->integer('player_id');
            $table->integer('year');
            $table->integer('player_age');
            $table->integer('belong_to');
            $table->integer('game');
            $table->integer('plate_appearance');
            $table->integer('at_bats');
            $table->integer('runs');
            $table->integer('hits');
            $table->integer('twobase');
            $table->integer('triple');
            $table->integer('homerun');
            $table->integer('rbi');
            $table->integer('fourball');
            $table->integer('intentional_walk');
            $table->integer('deadball');
            $table->integer('strikeout');
            $table->integer('sacrifice_bunt');
            $table->integer('sacrifice_fly');
            $table->integer('stolen_base');
            $table->integer('caught_stealing');
            $table->integer('doubleplay');
            $table->decimal('avg', 4, 3);
            $table->decimal('obp', 4, 3);
            $table->decimal('slg', 4, 3);
            $table->decimal('ops', 4, 3);
            $table->timestamps();
            $table->index('player_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_batting_results');
    }
}
