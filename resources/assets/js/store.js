const store = {
    debug: true,
    state: {
        user: null,
        team: null,
        mails: null,
        ranking_response: null,
        games_response: null,
        system_param: null,
        checked_is_logged_in: false,
        necessary_welcome_modal: false,
        waiting_by_clicking: false
    },
    updateUser () {
        return this.$http.get('/api/auth/user')
        .then(res => {
            this.state.user = res.data.user;
            return Promise.resolve();
        })
    },
    updateTeam () {
        let params = {};
        if (this.state.checked_is_logged_in === false) {
            params.login = true;
            this.state.checked_is_logged_in = true;
        }

        return this.$http.get('/api/auth/team', { params: params })
        .then(res => {
            this.state.team = res.data.team;
            if (_.has(res.data, 'is_accrued_login_bonus') && res.data.is_accrued_login_bonus === true) this.state.checked_is_logged_in = 'showLoginBonusModal';
            return Promise.resolve();
        })
    },
    updateMails() {
        return this.$http.get('/api/mail')
        .then(res => {
            this.state.mails = res.data.mails;
            return Promise.resolve();
        });
    },
    updateRanking () {
        let params = {};
        if (_.has(this.state.ranking_response, 'game_time')) params.game_time = this.state.ranking_response.game_time;
        return this.$http.get('/api/ranking', { params: params })
        .then(res => {
            if (res.data.modified === true) this.state.ranking_response = res.data;
            return Promise.resolve();
        })
    },
    updateGames () {
        let params = {};
        if (_.has(this.state.games_response, 'game_time')) params.game_time = this.state.games_response.game_time;
        return this.$http.get('/api/game', { params: params })
        .then(res => {
            if (res.data.modified === true) this.state.games_response = res.data;
            return Promise.resolve();
        })
    },
    updateSystemParam () {
        return this.$http.get('/api/system_param')
        .then(res => {
            this.state.system_param = res.data;
            return Promise.resolve();
        })
    },
    lockClicking () {
        this.waiting_by_clicking = true;
    },
    unlockClicking () {
        this.waiting_by_clicking = false;
    }
}
export default store;
