const Helper = {
    data() {
        return {
            constant: {
                team_order: {
                    order_type: { without_dh: 10, with_dh: 20 },
                    order_type_label: { without_dh: 'DHなし', with_dh: 'DHあり' }
                },
                positions: { pitcher: '投', catcher: '捕', first: '一', second: '二', third: '三', shortstop: '遊', left: '左', center: '中', right: '右', designated_hitter: '指'},
                position_labels: { pitcher: '投手', catcher: '捕手', first: '一塁手', second: '二塁手', third: '三塁手', shortstop: '遊撃手', left: '左翼手', center: '中堅手', right: '右翼手', designated_hitter: '指名打者'},
                roles: { dependable_pinch_hitter: '代打の切り札', second_pinch_hitter: '頼れる代打', third_pinch_hitter: '代打要因', utility: 'ユーティリティ', fielder: '守備固め', starter: '先発投手', long_relief: 'ロングリリーフ', short_relief: 'ワンポイント', pre_setup_pitcher: '準セットアッパー', setup_pitcher: 'セットアッパー', closer: '抑え' },
                batter_roles: [ 'dependable_pinch_hitter', 'second_pinch_hitter', 'third_pinch_hitter', 'utility', 'fielder' ],
                pitcher_roles: [ 'starter', 'long_relief', 'short_relief', 'pre_setup_pitcher', 'setup_pitcher', 'closer' ],
                all_player_count: 4821
            }
        }
    },
    methods: {
        jump(url) {
            location.href = url;
        },
        showScreen(screen_type) {
            this.screen_type = screen_type;
        },
        showModal(modal_type) {
            return this.$('.modal.' + modal_type).modal({ fadeDuration: 100, closeExisting: false });
        },
        toPositionStr(position) {
            return _.has(this.constant.positions, position) ? this.constant.positions[position] : '';
        },
        toPositionLabel(position) {
            return _.has(this.constant.position_labels, position) ? this.constant.position_labels[position] : '';
        },
        formatAvg(val) {
            if (!val) return '';
            val = Number(val);
            return (val >= 1) ? val.toFixed(3) : val.toFixed(3).slice(-1 * (val.toFixed(3).length - 1));
        },
        formatGetout(val) {
            if (!val) return '';
            return (val % 3 === 0) ? val / 3 : String(Math.floor(val / 3)) + '.' + String(val % 3);
        },
        toOrderTypeLabel(order_type) {
            let key = _.findKey(this.constant.team_order.order_type, (v) => v === order_type);
            return this.constant.team_order.order_type_label[key];
        },
        toRoleStr(role) {
            return _.has(this.constant.roles, role) ? this.constant.roles[role] : '';
        },
        makePaginatorMilestone(max_page) {
            const milestone_count = 4; // 両端+4で合計6個のボタンになる
            if (max_page < 10) return null; // 10ページない場合はページング数値ボタン不要

            let denom = milestone_count + 1;
            let milestone = [];
            milestone.push(1);
            milestone.push(Math.round(1 * max_page / denom));
            milestone.push(Math.round(2 * max_page / denom));
            milestone.push(Math.round(3 * max_page / denom));
            milestone.push(Math.round(4 * max_page / denom));
            milestone.push(max_page);

            return milestone;
        }
    }
}
export default Helper;
