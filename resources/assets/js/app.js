import Vue from 'vue'
import VueRouter from 'vue-router'
import Helper from './helper'
import Store from './store'

require('./bootstrap')

Vue.use(VueRouter)
Vue.component('layout', require('./components/Layouts/Layout.vue'))

Vue.mixin(Helper)

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: require('./components/Index.vue') },
        { path: '/menu', component: require('./components/Menu.vue') },
        { path: '/game/:id', component: require('./components/Game.vue') },
        { path: '/order', component: require('./components/Order.vue'), meta: { requiresTeam: true } },
        { path: '/scout', component: require('./components/Scout.vue'), meta: { requiresTeam: true } },
        { path: '/directory', component: require('./components/Directory.vue'), meta: { requiresTeam: true } },
        { path: '/ranking', component: require('./components/Ranking.vue') },
        { path: '/friend', component: require('./components/Friend.vue'), meta: { requiresUser: true } },
        { path: '/teamname', component: require('./components/Teamname.vue'), meta: { requiresTeam: true , requiresUser: true } },
        { path: '/sns', component: require('./components/Sns.vue'), meta: { requiresUser: true } },
        { path: '/team', component: require('./components/Team.vue'), meta: { requiresTeam: true } },
        { path: '/team/:id', component: require('./components/Team.vue'), props: true },
        { path: '/howto', component: require('./components/Howto.vue')},
    ]
})

const app = new Vue({
    data: {
        state: Store.state
    },
    methods: {
        updateTeam: Store.updateTeam,
        updateUser: Store.updateUser,
        updateMails: Store.updateMails,
        updateRanking: Store.updateRanking,
        updateGames: Store.updateGames,
        updateSystemParam: Store.updateSystemParam,
        lockClicking: Store.lockClicking,
        unlockClicking: Store.unlockClicking,
    },
    router,
    el: '#app'
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresTeam) && _.isEmpty(app.state.team)) {
        next({ path: '/' });
    } else if (to.matched.some(record => record.meta.requiresUser) && _.isEmpty(app.state.user)) {
        next({ path: '/' });
    } else {
        next();
    }
})

