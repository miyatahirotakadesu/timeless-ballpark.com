<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108353131-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
                  gtag('js', new Date());

          gtag('config', 'UA-108353131-1');
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TimelessBallpark</title>
        <meta name="description" content="プロ野球選手の年度別成績を元にチームを作るブラウザで遊ぶ無料の野球ゲーム。全盛期の選手たちを揃えて最強のチームを作ろう。" />
        <meta name="keywords" content="野球,ゲーム,無料" />

        <meta property="og:title" content="TimelessBallpark" />
        <meta property="og:type" content="game" />
        <meta property="og:url" content="{{ env('APP_URL') }}" />
        <meta property="og:image" content="{{ env('APP_URL') }}/ballpark.png" />
        <meta property="og:site_name"  content="TimelessBallpark" />
        <meta property="og:description" content="プロ野球選手の年度別成績を元にチームを作るブラウザで遊ぶ無料の野球ゲーム。全盛期の選手たちを揃えて最強のチームを作ろう。" />
         
        <meta property="fb:app_id" content="284628742029633" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="TimelessBallpark" />
        <meta name="twitter:url" content="{{ env('APP_URL') }}" />
        <meta name="twitter:description" content="プロ野球選手の年度別成績を元にチームを作るブラウザで遊ぶ無料の野球ゲーム。全盛期の選手たちを揃えて最強のチームを作ろう。" />
        <meta name="twitter:image" content="{{ env('APP_URL') }}/ballpark.png" />

        <link rel="icon" href="/favicon.ico">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}"></script>
        <link rel="stylesheet" href="{{ mix('css/styles.css') }}"></script>

        <script>
            window.Laravel = {
                csrfToken: "{{ csrf_token() }}"
            };
            window.app_url = '{{ env('APP_URL') }}';
        </script>
    </head>
    <body>
        <div id="app">
            <layout></layout>
        </div>
    </body>
    <script src="{{ mix('js/app.js') }}"></script>
</html>

